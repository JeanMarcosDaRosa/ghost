clear
echo "=================="
echo "___---*** Ghost - MondoDB ***---___"
echo "Exportar 1"
echo "Importar 2"
echo "ctrl+c -> Sair"

read opcao

case $opcao in
	1)	echo "Exportando Dados"
		mongoexport --db ghost --collection channels --out channels.json
		mongoexport --db ghost --collection posts --out posts.json
		mongoexport --db ghost --collection to_posting --out to_posting.json
		mongoexport --db ghost --collection  users --out users.json
		;;
	2) 	echo "Importando Dados"
		mongoimport --db ghost --collection channels --file channels.json
		mongoimport --db ghost --collection posts --file posts.json
		mongoimport --db ghost --collection to_posting --file to_posting.json
		mongoimport --db ghost --collection users --file users.json
		;;
	*) echo "Opcao Invalida!";;
esac

