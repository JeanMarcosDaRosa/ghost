import os
import pymongo
from urllib2 import urlopen
from simplejson import loads
import sys

#Servidor Ghost
client = pymongo.MongoClient("127.0.0.1", 27017)
db = client.ghost

def getPage(url_entry, token):
	content = False
	try:
		content = loads(urlopen(url_entry).read())
	except:
		try:
			content = loads(urlopen(url_entry+token).read())
		except Exception, e:
			print 'Err page: ', url_entry, ', Error: ', e
	return content

f = open('lista_canais.txt')
lines = f.readlines()
f.close()

#db.channels.remove()

#at = raw_input('Informe o Acess Token: ')
at = ""
if len(sys.argv) == 2:
	at = sys.argv[1]
else:
	print "\n**** Access Token Required!!***\n"
	sys.exit(1)

for ch in lines:
	username = ch.strip()
	if not db.channels.find_one({"username":username}):
		content = getPage('https://graph.facebook.com/'+username, at)
		if content:
			cat = content['category']
			name = content['name']
			db.channels.save({"username": username, "name": name, "description": cat})
if not db.channels.find_one({"username":"friends"}):
	db.channels.save({"username": "friends", "name": "__Amigos__", "description": "Postagem de usuario"})
cont = 0
for item in db.channels.find():
	print 'ID: ', item["_id"], ', Username: ', item["username"], ', Description: ', item["description"]
	cont+=1

print "\n=============\nTotal de ", cont , " canais"

