# Welcome to Social Ghost project

Social Ghost is a social network application and integrator of various channels of content, such as RSS, Email, Facebook Fan Pages, among others, making it a relevant content to the user environment, through semantic processing of text and user interests

## Download of Android application

To application release download: [Web Site](http://jeanmarcos.com.br/ghost.apk)

# Install/Configuration Server

To configuration and run on your linux workstation:

### Install MongoDB
```
echo 'deb http://downloads-distro.mongodb.org/repo/debian-sysvinit dist 10gen' | tee /etc/apt/sources.list.d/mongodb.list
apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
apt-get -y update
apt-get -y install mongodb-10gen
service mongodb start
```

### Install Cordova / Ant for Build App
```
npm install -g cordova
apt-get install ant
```

### Install Apache2
```
apt-get install apache2
```

### Install NodeJs/Nodemon and Dependencies
```
apt-get install nodejs
apt-get install libmagick++-dev
apt-get install imagemagick
npm install -g nodemon
ln -s /usr/bin/nodejs /usr/bin/node
```

### Install Python/Pip and Module Goslate for translations
```
apt-get install python-pip python-dev build-essential
pip install --upgrade pip
pip install goslate
```

### Install FFmpeg (Last Version)
```
/path/for/ghost/res/tools/install_ffmpeg.sh
```

### Configure GIT and clone repository the ghost project
```
git config --global credential.helper 'cache --timeout=3600000'
git config --global user.name "Your Name"
git config --global user.email "your.mail@example.com"
git clone https://YourUsername@bitbucket.org/JeanMarcosDaRosa/ghost.git
```

### Configuration of apache2 for access from your browser (Optional):

Copy default configuration file
```
cd /etc/apache2/sites-available
cp 000-default.conf localhost.com.conf
```

Change values for `ServerName localhost.com` and `DocumentRoot /path/for/ghost/app/platform/android/assets/www`
```
nano localhost.com.conf
```

Enable new configurations and restart apache2
```
a2ensite localhost.com.conf
service apache2 restart
```

Access on your browser: `http://localhost.com`

### Start Ghost Server on ports 8080 (REST) and 8081 (http)
```
cd /path/for/ghost/server/
nodemon ghostserver.js --debug --enable-status --enable-update --hash [a password for status/update request]
```

Thanks for collaborate! For more details, access the Wiki.