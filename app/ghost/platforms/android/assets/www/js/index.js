var addressServer = "http://162.243.69.60:8080";
var listFriendsInApp = [];
var listFriendsOutApp = [];
var listAllFriends = [];
var lookConnections = true;
var passEnterApp = false, passSyncContacts = false, syncContacts = false, passDevReady = false, passI18n = false;
var listContactsFromDevice = [];
var ultimoPostEnviado = "";
var urlRegex = /(((ftp|https?):\/\/)[\-\w@:%_\+.~#?,&\/\/=]+)|((mailto:)?[_.\w-]+@([\w][\w\-]+\.)+[a-zA-Z]{2,3})/g;
var youtubeRegex = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
var vimeoRegex = /^http:\/\/(www\.)?vimeo\.com\/(clip\:)?(\d+).*$/;
var globalConfigs=[];
var listPosts = [];
var myPosts = [];
var sizesVideo = [ [3840,2160], [2560,1440], [1920,1080], [1280,720], [854,480], [640,360], [426,240] ];
var diffTime = 0;

function isDeviceMobile(){
    console.debuglog("Função isDeviceMobile");
    return (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/));
}

function onErrorPlugin() {
    console.debuglog("Erro ao carregar algum plugin cordova");
    alert(i18n.t('alertas.esteDispositivoNaoSuportaOAplicativo'));
    console.debuglog("Saindo do aplicativo em decorrencia do erro");
    navigator.app.exitApp();
}

var onMinimize = false;
var telephoneNumber;
//var titleBackgroud = "Social Ghost";
//var msgBackground = "Working for the best content for you";

console.debuglog = function(object){ if(DEBUG) console.debug(object); }

var app = {

    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        if (isDeviceMobile()) {
            document.addEventListener('deviceready', this.onDeviceReady, false);
        }
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        console.debuglog("Função do evento onDeviceReady");
        passDevReady = true;
        app.receivedEvent('deviceready');
        window.cache.clear(function(status){
            console.debuglog("Função de sucesso ao limpar cache do cordova");
            console.debuglog(status);
        }, function(status){
            console.debuglog("Função de falha ao limpar cache do cordova");
            console.debuglog(status);
        });
        window.plugin.notification.local.onclick = function (id, state, json) {
            console.debuglog("Retornou da notificacao: "+id+", post: "+JSON.parse(json).idpost);
            scrollToAnchor('post_channelName_'+JSON.parse(json).idpost);
        }
        // Called when background mode has been activated
        /*cordova.plugins.backgroundMode.onactivate = function () {
            console.debuglog('backgroundMode: onactivate');
            setTimeout(function () {
                console.debuglog('backgroundMode: trigger');
                // Modify the currently displayed notification
                cordova.plugins.backgroundMode.configure({
                    title: titleBackgroud,
                    text: msgBackground
                });
            }, 5000);
        }
        enabeBackgroundMode();*/
        document.addEventListener("backbutton", function(e){
            console.debuglog("Função do evento backbutton");
            e.preventDefault();
            if($(".secNav").is(':visible') && window.localStorage.getItem('fnome')!=="undefined" && window.localStorage.getItem('fnome')!==""){
                console.debuglog("Encondendo menu de configurações");
                $('#sidebar').hide();
                $(".secNav").hide();
            }else if($("#dialogPost").is(':visible') || $("#dialogContact").is(':visible')){
                console.debuglog("Fechando dialogs de Post e Novo Contato");
                closePostDialog();
                closeContactDialog();
            }else if($('.fancybox-wrap').size()>0){
                console.debuglog("Função visualização de imagem");
                $.fancybox.close();
            }else{
                if($(location).attr('href').indexOf('canais.html')>-1){
                    console.debuglog("Voltando para o index");
                    window.location="index.html";
                }else{
                    console.debuglog("Minimizando aplicativo");
                    navigator.Backbutton.goHome(function() {
                        console.debuglog('Sucesso na minimização');
                    }, function() {
                        console.debuglog('Falha na minimização');
                    });
                }
            }
        }, false);

        document.addEventListener("offline", function() {
            console.debuglog("Dispositivo ficou offline");
            hideLoader('loader');
            navigator.splashscreen.hide();
            showAlert('nFailure', i18n.t("alertas.falhaConexao"));
        });

        document.addEventListener("online", function() {
            console.debuglog("Dispositivo ficou online");
            showAlert('nSuccess', i18n.t("alertas.retornoConexao"), 5);
            if(!passEnterApp){
                window.location.reload();   
            }
        });

        document.addEventListener("resume", function() {
            console.debuglog("Função do evento resume");
            onMinimize = false;
        }, false);

        document.addEventListener("pause", function(evt){
            console.debuglog("Função do evento pause");
            onMinimize = true;
        }, false);
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.debuglog("Função do evento receivedEvent");
        console.debuglog("Carregando plugin telephonenumber");
        telephoneNumber = cordova.require("cordova/plugin/telephonenumber");
        console.debuglog("Chamando função telephoneNumber.getEmail");
        telephoneNumber.getEmail(function(resultMail) {
            if(resultMail===''){
                console.debuglog("Falhou ao buscar email do usuário com a função telephoneNumber.getEmail");
                alert(i18n.t('alertas.falhaGetEmail'));
                console.debuglog("Saindo do aplicativo em função da falha em telephoneNumber.getEmail");
                navigator.app.exitApp();
            }else{
                console.debuglog("Coletou email: "+resultMail);
                window.localStorage.setItem('email', resultMail);
                $('#showIdUser').text(resultMail);
                var infoDevice = 'Device Model: '    + device.model + ', Device Platform:' + device.platform + ', Device Version: ' + device.version;
                window.localStorage.setItem('device', infoDevice);
                console.debuglog('Informações do dispositivo: '+infoDevice);
                var resultSerial = device.uuid;
                if(resultSerial==undefined || resultSerial==='' || resultSerial==='NULL'){
                    console.debuglog("Falhou ao buscar serial do dispositivo com a função telephoneNumber.getSerial");
                    alert(i18n.t('alertas.falhaGetSerial'));
                    console.debuglog("Saindo do aplicativo em função da falha em telephoneNumber.getSerial");
                    navigator.app.exitApp();
                }else{
                    console.debuglog("Coletou serialdevice: "+resultSerial);
                    window.localStorage.setItem('serialdevice', resultSerial);
                    onLoadPG();
                    console.debuglog("Chamando função telephoneNumber.getListContacts");
                    telephoneNumber.getListContacts(function(resultContacts) {
                        console.debuglog("Coletou a lista de contatos: ");
                        console.debuglog(resultContacts);
                        listContactsFromDevice=resultContacts;
                        passSyncContacts=true;
                        if(passEnterApp){
                            console.debuglog("Detectou que o fluxo já passou pela função onLoadPG");
                            syncContacts=true;
                            console.debuglog("Enviando contatos encontrados para o servidor no fluxo de telephoneNumber.getListContacts");
                            saveContacts(resultContacts, carregaListaContatos);
                        }
                    }, onErrorPlugin);
                }
            }
        }, onErrorPlugin);
    }
};

function scrollToAnchor(aid){
    var aTag = $("a[id='"+ aid +"']");
    if(aTag) $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

var refreshEventsImageGallery = function(){
    console.debuglog("Função refreshEventsImageGallery");
    $("[rel=tooltip]").tooltip();
    
    $("a.group").fancybox({
        'titleShow'     : false,
        type        : 'image',
        openEffect : 'elastic',
        openSpeed  : 150,
        closeEffect : 'elastic',
        closeSpeed  : 150,
        closeClick : true,
        margin : [60, 20, 0, 20],
        'titlePosition'     : 'over',
        'titleFormat'       : function(title, currentArray, currentIndex, currentOpts) {
            return '<span id="fancybox-title-over">' +  (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
        },
        'afterLoad': function() {
            //$(document).scrollTop(0);
            //$(".fancybox-wrap").css({'top':'60px', 'max-heigth': '100%', 'bottom':'auto'});
        },
    });
}

var showAlert = function(type, msg, timer){
    console.debuglog("Função showAlert");
    $('#notificacoes').hide();
    $('#notificacoes').empty();
    $('#notificacoes').removeClass( "nWarning nInformation nSuccess nFailure" ).addClass( type );
    $('#notificacoes').append('<p>'+msg+'</p>');
    $('#notificacoes').fadeIn(300);
    if(timer){
        console.debuglog("Função showAlert recebeu um timer de "+timer+" segundos");
        var vHideAlert = setInterval(function(){
            $('#notificacoes').fadeOut(800);
        }, (timer*1000));
    }
}

var sobrescreverSubmitSearch = function(){
    console.debuglog("Função sobrescreverSubmitSearch");
    $('.userSearch form').on("submit",cancelBubble);
    $('.topSearch form').on("submit",cancelBubble);
    $('.userSearch input[type=submit]').on("click", function(){searchSemantic($(".userSearch input[type='text']").val())});
    $('.topSearch input[type=submit]').on("click", function(){searchSemantic($(".topSearch input[type='text']").val())});
}

var loadConfigurations = function(callback){
    console.debuglog("Função loadConfigurations");
    var email = window.localStorage.getItem('email');
    console.debuglog("Buscando lista de configurações do usuário");
    ajaxNodeJs(addressServer, 'listconfigs', 'GET', null, function(usuario){
        console.debuglog("Nome do usuário: "+usuario.response.first_name);
        window.localStorage.setItem('fnome', usuario.response.first_name );
        window.localStorage.setItem('fullname', usuario.response.first_name );
        configuracoes=usuario.response.configs;
        console.debuglog("Configurações encontradas: ");
        console.debuglog(configuracoes);
        $('#fnome').val(usuario.response.first_name);
        $('#ch1').attr('checked', (configuracoes['auto_comment']));
        $('#ch2').attr('checked', (configuracoes['btn_refresh_post']));
        $('#ch3').attr('checked', (configuracoes['posts_images']));
        $('#ch4').attr('checked', (configuracoes['posts_video']));
        $('#ch5').attr('checked', (configuracoes['btn_translate']));
        $('#ch6').attr('checked', (configuracoes['btn_subir']));
        $("#select_posts_refresh option").each(function() { 
            this.selected = (this.value === configuracoes['posts_for_refresh'].toString()); 
        });
        $("#select_themes option").each(function() { 
            this.selected = (this.value === configuracoes['theme'].toString()); 
        });
        $('#select_posts_refresh').change();
        $('#pais').change();
        $('#select_themes').change();
        $('#ch1').change();
        $('#ch2').change();
        $('#ch3').change();
        $('#ch4').change();
        $('#ch5').change();
        $('#ch6').change();
        globalConfigs = {posts_for_refresh : configuracoes['posts_for_refresh'], auto_comment : configuracoes['auto_comment'], btn_refresh_post : configuracoes['btn_refresh_post'], 
                         btn_translate: configuracoes['btn_translate'], btn_subir : configuracoes['btn_subir'], posts_images : configuracoes['posts_images'], posts_video : configuracoes['posts_video'], theme : configuracoes['theme']};
        if(window.localStorage.getItem('fnome')==="undefined" || window.localStorage.getItem('fnome')===""){
            $('#sidebar').slideToggle(100);
            $('.secNav').slideToggle(200);
            $('#anchorConfUser').click();
        }
        callback();
    }, function (response){ 
        console.debuglog("Falha ao buscar lista de configurações do usuário");
        console.debuglog(response); 
    }, null);
}

var searchSemantic = function(text){
    console.debuglog("Função searchSemantic");
    var email = JSON.parse(window.localStorage.getItem('email'));
    dados = { info_text: text };
    $('.topSearch').hide();
    $('#sidebar').hide();
    console.debuglog("Buscando postagens com a função searchSemantic");
    ajaxNodeJs(addressServer, 'search', 'GET', null, function (response){
        console.debuglog("Sucesso ao realizar pesquisa com searchSemantic");
        $('.topAlertSearch').fadeIn(1000).delay(2000).fadeOut(1500);
        console.debuglog(response);
    }, function (response){
        console.debuglog("Falha ao realizar pesquisa com searchSemantic");
        console.debuglog(response); 
    }, dados);
}

var checkConnection = function(){
    console.debuglog("Função checkConnection");
    if(window.cordova){
        var networkState;
        var test = cordova.exec(
            function(winParam) { networkState = winParam; },
            function(error) {alert("Network Manager error: "+error);},
            "NetworkStatus",
            "getConnectionInfo",
            []
        );
        return networkState;
    }else{
        return "unknow";
    }
}

var saveContacts = function(listContacts, callback){
    console.debuglog("Função saveContacts");
    dados = { list_contacts: listContacts };
    console.debuglog("Salvando contatos no servidor: ");
    console.debuglog(listContacts);
    ajaxNodeJs(addressServer, 'savecontacts', 'POST', null, function (response){  
        console.debuglog("Sucesso ao salvar lista de contatos no servidor");
        syncContacts = true;
        callback();
    }, function (response){
        console.debuglog("Falha ao salvar lista de contatos no servidor");
        syncContacts = false;
    }, dados);
}

var saveConfigs = function(){
    console.debuglog("Função saveConfigs");
    var fnome = $('#fnome').val();
    if(fnome!=undefined && fnome!=='' && fnome!=null){
        var v1 = ($('#ch1').attr('checked')!= undefined);
        var v2 = ($('#ch2').attr('checked')!= undefined);
        var v3 = ($('#ch3').attr('checked')!= undefined);
        var v4 = ($('#ch4').attr('checked')!= undefined);
        var v5 = ($('#ch5').attr('checked')!= undefined);
        var v6 = ($('#ch6').attr('checked')!= undefined);
        var pt = $('#select_posts_refresh').val();
        var th = $('#select_themes').val();
        dados = { info_fname: fnome, posts_for_refresh: pt, auto_comment: v1, btn_refresh_post: v2, posts_images: v3, posts_video: v4, btn_translate: v5, btn_subir: v6, theme: th, lang: i18n.lng() };
        console.debuglog("Salvando configurações no servidor: ");
        console.debuglog(dados);
        ajaxNodeJs(addressServer, 'saveconfigs', 'POST', null, function (response){
            console.debuglog("Sucesso ao salvar configurações no servidor");
            if (confirm(i18n.t("confirmacoes.atualizaPagina"))) {
                window.location.reload();
            } else {
                $(".secNav").toggleClass('display');
            }
        }, function (response){ 
            console.debuglog("Falha ao salvar configurações no servidor");
            console.debuglog(response); 
        }, dados);
    }else{
        console.debuglog("Usuário omitiu primeiro nome");
        showAlert('nWarning', i18n.t("alertas.informePrimeiroNome"));
    }
}

var ajaxNodeJs = function(serverAddress, chamado, typeAjax, loader, acaoSucesso, acaoFalha, dados){
    console.debuglog("Função ajaxNodeJs");
    console.debuglog("Request Ajax: '"+serverAddress+"/"+chamado+"' -> Data:");
    console.debuglog(dados);

    var serial = window.localStorage.getItem('serialdevice');
    var userid = window.localStorage.getItem('email');
    var infoDevice = window.localStorage.getItem('device');

    if(dados==null) dados = {};
    dados['serialdevice']= serial;
    dados['userid']= userid;
    dados['device']= infoDevice;
    dados['build']= BUILD_NUMBER;
    for(var i in dados){
        if($.isArray(dados[i])){
            console.debuglog(i+" é um array");
            dados[i] = replaceAll($.base64.btoa(JSON.stringify(dados[i])), "=", "");
        }else{
            dados[i] = replaceAll($.base64.btoa(dados[i]), "=", "");
        }
    }
    if(loader!=null && loader!=undefined) showLoader(loader);
    if(typeAjax==='POST'){
        $.post(serverAddress+'/'+chamado, dados, function(json){
            console.debuglog("Sucesso no POST "+chamado);
            acaoSucesso(json, loader);
        }, "json").fail(function(e) {
            console.debuglog("Falha no POST "+chamado);
            showAlert('nFailure', i18n.t("alertas.falhaConexao"), 5);
            hideLoader('loader');
            hideLoader(loader);
            acaoFalha(e);
        });
    }else{
        $.ajax({
            type: 'GET',
            url: serverAddress+'/'+chamado+'?callback=?',
            async: false,
            jsonpCallback: 'jsonCallback'+chamado,
            contentType: "application/json",
            dataType: 'jsonp',
            data: dados
        }).done(function(json){
            console.debuglog("Sucesso no GET "+chamado);
            if(json!=undefined && json!=null && (json.ACCESS==undefined || (json.ACCESS!=='INVALID_TOKEN'))
                && (json.ACCESS==undefined || (json.ACCESS!=='INVALID_BUILD'))){
                acaoSucesso(json, loader);
            }else{
                lookConnections=true;
                if(json.ACCESS!=undefined && json.ACCESS==='INVALID_BUILD'){
                    showAlert('nFailure', i18n.t("alertas.falhaBuild"));
                }else{
                    showAlert('nFailure', i18n.t("alertas.falhaAutentificacaoPart1")+window.localStorage.getItem('email')+i18n.t("alertas.falhaAutentificacaoPart2"));
                }
                hideLoader('loader');
                hideLoader(loader);
            }
        }).fail(function(e){
            console.debuglog("Falha no GET "+chamado);
            if(chamado!=="newposts" && chamado!=="lastposts" && chamado!=="postsConfirm"){
                console.debuglog("Exibindo mensagem ao usuário, em decorrencia da falha no chamado: "+chamado);
                showAlert('nFailure', i18n.t("alertas.falhaConexao"), 5);
            }
            hideLoader('loader');
            hideLoader(loader);
            acaoFalha(e);
        }); //, timeout: 3000
    }
    
}

function setPerfil(){
    console.debuglog("Função setPerfil");
    carregarPerfil();
    ajustaDivConfig();
}

function cancelBubble(e) {
    console.debuglog("Função cancelBubble");
    var evt = e ? e:window.event;
    if (evt.stopPropagation)    evt.stopPropagation();
    if (evt.cancelBubble!=null) evt.cancelBubble = true;
    evt.preventDefault(); // prevents default
    return false;
}

function entrarAPP(sucesso, error){
    console.debuglog("Função entrarAPP");
    //envia informacoes do acesso ao servidor e redireciona para o index.html
    console.debuglog("Chamando a o registro de acesso no servidor com o chamado accessRegister");
    ajaxNodeJs(addressServer, 'accessRegister', 'GET', null, sucesso, error, null);
}

function loadScript(url){
    console.debuglog("Função loadScript");
    var xhrObj =  new XMLHttpRequest();
    xhrObj.open('GET', url, false);
    xhrObj.send('');
    var se = document.createElement('script');
    se.text = xhrObj.responseText;
    document.getElementsByTagName('head')[0].appendChild(se);
    app.initialize();
}

function loadPicturePerfil(userid, div){
    console.debuglog("Função loadPicturePerfil");
    /*Usa ajax para testar se a imagem existe no servidor, pois se nao existir
      nao ira substituir a existente na div 
    */
    $.ajax({
        type: 'GET',
        url: addressServer+'/public/upload/'+CryptoJS.MD5(userid),
        async: false,
        data: { dim:'50x50' },
        success: function(result){
            console.debuglog("Sucesso ao buscar imagem de perfil");
            $('#'+div).attr('src', addressServer+'/public/upload/'+CryptoJS.MD5(userid)+'?dim=50x50');
        }, error: function(e) {
            console.debuglog("Falhou ao buscar imagem de perfil");
            console.debuglog(e);
        }
    });
}

var clickUpload = function (inputDiv, imgDiv){
    console.debuglog("Função clickUpload");
    if(isDeviceMobile()){
        console.debuglog("Detectou que se trata de um dispositivo movel");
        console.debuglog("Chamando recurso de câmera pra abrir arquivos do dispositivo");
        navigator.camera.getPicture(function(imageURI){
            console.debuglog("Sucesso ao abrir recurso de camera e buscar arquivo, retornou uma URI:");
            console.debuglog(imageURI);
            uploadPhoto(imageURI, inputDiv, imgDiv);
        }, function(message) {
            console.debuglog("Falha ao buscar imagem");
            //alert(i18n.t('alertas.falhaPegarImagem')); 
        }, { quality: 75, destinationType: navigator.camera.DestinationType.FILE_URI, sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY, mediaType: navigator.camera.MediaType.ALLMEDIA });
    }
}

var ajustInputImgPost = function(){
    console.debuglog("Função ajustInputImgPost");
    $('#newPostURL_IMG').css({'width': ($('#dialogPost').width()-($('#addImgPost').width()+80))+'px'});
}


var onLoadImg = function(evt) {
    console.debuglog("Função onLoadImg");
    console.debuglog("Carregou imagem de pre-visualização");
    var id = replaceAll($.base64.btoa(new Date().getTime()), "=", "");
    var $li = $('<li id="liIMG_'+id+'""     data-hash="'+$(evt.target).data('hash')+'"/>').append(evt.target);
    $li.append('<a onclick="removeIMGPost(\''+id+'\');" data-i18n="[title]botao.remover" class="buttonRemoveImg"><img src="images/icons/delete_comment.png" alt=""></a>');
    $('#preloadPost').append($li);
    $("#newPostURL_IMG").val("");
    $('#chboxUploadLink').prop( "checked", false );
    $('#chboxUploadLink').parent().removeClass('checked');
}

var onErrorImg = function(evt) {
    console.debuglog("Função onErrorImg");
    console.debuglog("Erro ao carregar imagem de pre-visualização");
    //$('#preload').attr("src", "images/clear.png");
    evt.preventDefault();
    var link = $(findUrls($("#newPostURL_IMG").val())).get(0) || "";
    var youtubeUrl = link.match(youtubeRegex);
    var vimeoUrl = link.match(vimeoRegex);
    if(youtubeUrl || vimeoUrl){
        console.debuglog("Detectou um video e esta tentando carregar a imagem de representação de videos: icon-video.png");
        $(evt.target).attr("src", "images/icons/icon-video.png");
        $('#chboxUploadLink').prop( "checked", true );
        $('#chboxUploadLink').parent().addClass('checked');
    }else{
        alert("Link de conteúdo é inválido");
    }
}


var sendMediaToServer = function(imageURI, typeMedia){
    console.debuglog("Função sendMediaToServer");
    console.debuglog("param typeMedia: "+typeMedia);
    var options = new FileUploadOptions(), params = new Object();
    options.fileKey="file";
    options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
    options.mimeType=typeMedia;
    //options.mimeType = "video/mov";
    options.chunkedMode = false;
    params.userid = $.base64.btoa(window.localStorage.getItem('email'));
    params.serialdevice = $.base64.btoa(window.localStorage.getItem('serialdevice'));
    params.build = $.base64.btoa(BUILD_NUMBER);
    params.typemedia = $.base64.btoa(typeMedia);
    options.params = params;
    var ft = new FileTransfer();
    ft.onprogress = function(progressEvent) {
        if (progressEvent.lengthComputable) {
            var $per = ((progressEvent.loaded / progressEvent.total)*100);
            $('#progressBarPost').css({width: $per.toFixed(2)+"%"});
        }
    };
    console.debuglog("Realizando upload");
    $('#progressBarPost').show();
    ft.upload(imageURI, encodeURI(addressServer+"/uploadpost"), function(result){
        $('#progressBarPost').hide();
        $('#progressBarPost').css({width: "0%"});
        var response=JSON.parse(result.response);
        if(response!=undefined && response!=null && (response.ACCESS==undefined || (response.ACCESS!=='INVALID_TOKEN'))){
            console.debuglog("Sucesso no upload");
            var link = '[SERVER]/public/'+response.upload.file;
            console.debuglog("url gerada: "+link);
            
            if(response.upload.type==="image/jpeg"){
                $('<img class="preload" data-url="'+link+'" data-type="photo" data-hash="'+response.upload.hash+'" />').attr("src", $('#inputFilePost').data('uri') ).load(onLoadImg);
            }else{
                $('<img class="preload" data-url="'+link+'" data-type="video" data-hash="'+response.upload.hash+'" />').attr("src", addressServer+'/public/'+response.upload.file+'-thumb.png' ).load(onLoadImg);
            }

            $('#imgToPost').css('background-image', '');
            $('#inputFilePost').val("");
            $('#inputFilePost').data('uri', '');
            $('#labelTargetPost').removeClass('dropped');
            //$('<img class="preload" data-url="'+link+'" />').attr("src", addressServer+'/public/'+response.upload).load(onLoadImg);
        }else{
            console.debuglog("Falha no upload");
            if(response.ACCESS==undefined || response.ACCESS!=='INVALID_TOKEN'){
                lookConnections=true;
                showAlert('nFailure', i18n.t("alertas.falhaAutentificacaoPart1")+window.localStorage.getItem('email')+i18n.t("alertas.falhaAutentificacaoPart2"));
            }else{
                showAlert('nFailure', i18n.t("alertas.falhaConexao"), 5);
            }
        }
    }, function(res){
        console.debuglog("Falha no upload");
        showAlert('nFailure', i18n.t("alertas.falhaUpload"), 5);
    }, options);
}

var enviarImagemPerfil = function(){
    console.debuglog("Função enviarImagemPerfil");
    if(isDeviceMobile()){
        console.debuglog("Detectou que se trata de um dispositivo movel");
        var imageURI = $('#inputFileSend').data('uri');
        if(imageURI!=undefined && imageURI!=null && imageURI!==""){
            $('<img/>').attr('src', imageURI).load(function() {
                $(this).remove(); // prevent memory leaks as @benweet suggested
                console.debuglog("imageURI é diferente de undefined");
                //TODO: testar
                window.resolveLocalFileSystemURI(imageURI, function(fileEntry) {
                    fileEntry.file(function(fileObj) {
                        console.log("Size = " + fileObj.size);
                        if(fileObj.size<=(5*1024*1024)){
                            var options = new FileUploadOptions(),
                                params = new Object();
                            options.fileKey="file";
                            options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
                            options.mimeType="image/jpeg";
                            options.chunkedMode = false;
                            params.userid = $.base64.btoa(window.localStorage.getItem('email'));
                            params.serialdevice = $.base64.btoa(window.localStorage.getItem('serialdevice'));
                            params.build = $.base64.btoa(BUILD_NUMBER);
                            options.params = params;
                            var ft = new FileTransfer();
                            ft.onprogress = function(progressEvent) {
                                if (progressEvent.lengthComputable) {
                                    var $per = ((progressEvent.loaded / progressEvent.total)*100);
                                    $('#progressBarSend').css({width: $per.toFixed(2)+"%"});
                                }
                            };
                            console.debuglog("Chamou o upload");
                            $('#progressBarSend').show();
                            ft.upload(imageURI, encodeURI(addressServer+"/upload"), function(result){
                                $('#progressBarSend').hide();
                                $('#progressBarSend').css({width: "0%"});
                                console.debuglog("Sucesso ao realizar upload");
                                console.debuglog(result.response);
                                var response=JSON.parse(result.response);
                                if(response!=undefined && response!=null && (response.ACCESS==undefined || (response.ACCESS!=='INVALID_TOKEN'))){
                                    window.location.reload();
                                }else{
                                    lookConnections=true;
                                    console.debuglog("Falha ao realizar upload por falha de autenticação");
                                    showAlert('nFailure', i18n.t("alertas.falhaAutentificacaoPart1")+window.localStorage.getItem('email')+i18n.t("alertas.falhaAutentificacaoPart2"));
                                }
                            }, function(res){
                                console.debuglog("Falha ao realizar upload");
                                showAlert('nFailure', i18n.t("alertas.falhaCompartilharPost"), 10);
                            }, options);
                        }else{
                            alert(i18n.t("alertas.tamanhoImagemExcedido"));
                        }
                    });
                });
            }).error(function(){
                console.debuglog("Falha ao renderizar imagem");
                showAlert('nFailure', i18n.t("alertas.imagemInvalida"), 3);
            });
        }
    }else{
        console.debuglog("Detectou que não se trata de um dispositivo movel");
        if($('#inputFileSend').val()!==""){
            var input = document.getElementById('inputFilePost');
            //if(input.files[0].size<=(5*1024*1024)){
                $('#sendFileSend').ajaxForm({
                    beforeSend: function() {
                        console.debuglog("Realizando upload da imagem");
                        $('#progressBarSend').show();
                        $('#progressBarSend').css({width: "0%"});
                    },
                    uploadProgress: function(event, position, total, percentComplete) {
                        $('#progressBarSend').css({width: percentComplete + '%'});
                    },
                    complete: function(){
                        $('#progressBarSend').hide();
                        $('#progressBarSend').css({width: "0%"});
                    },
                    success: function (response) {
                        $('#progressBarSend').hide();
                        $('#progressBarSend').css({width: "0%"});
                        if(response!=undefined && response!=null && (response.ACCESS==undefined || (response.ACCESS!=='INVALID_TOKEN'))){
                            window.location.reload();
                        }else{
                            console.debuglog("Falha no upload da imagem");
                            if(response.ACCESS==undefined || response.ACCESS!=='INVALID_TOKEN'){
                                lookConnections=true;
                                console.debuglog("Falha ao realizar upload por falha de autenticação");
                                showAlert('nFailure', i18n.t("alertas.falhaAutentificacaoPart1")+window.localStorage.getItem('email')+i18n.t("alertas.falhaAutentificacaoPart2"));
                            }else{
                                showAlert('nFailure', i18n.t("alertas.falhaConexao"), 5);
                            }
                        }
                    }
                });
                $('#sendFileSend').submit();
            //}else{
            //    alert(i18n.t("alertas.tamanhoImagemExcedido"));
            //}
        }
    }
}

var uploadImagemToPost = function(){
    console.debuglog("Função uploadImagemToPost");
    if(isDeviceMobile()){
        console.debuglog("Detectado que se trata de um dispositivo movel");
        var imageURI = $('#inputFilePost').data('uri');
        if(imageURI!=undefined && imageURI!=null && imageURI!==""){
            console.debuglog("Recurso de upload retornou uma URI valida");
            $('<img/>').attr('src', imageURI).load(function() {
                $(this).remove(); // prevent memory leaks as @benweet suggested
                sendMediaToServer(imageURI, "image/jpeg");
            }).error(function(){
                sendMediaToServer(imageURI, "video/mov");
            });
        }else{
            console.debuglog("Recurso de upload não retornou uma URI valida");
        }
    }else{
        console.debuglog("Detectado que não se trata de um dispositivo movel");
        if($('#inputFilePost').val()!==""){
            $('#sendFilePost').ajaxForm({
                beforeSend: function() {
                    console.debuglog("Realizando upload da imagem");
                    $('#progressBarPost').show();
                    $('#progressBarPost').css({width: "0%"});
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    $('#progressBarPost').css({width: percentComplete + '%'});
                },
                complete: function(){
                    $('#progressBarPost').hide();
                    $('#progressBarPost').css({width: "0%"});
                },
                success: function (response) {
                    $('#progressBarPost').hide();
                    $('#progressBarPost').css({width: "0%"});
                    if(response!=undefined && response!=null && (response.ACCESS==undefined || (response.ACCESS!=='INVALID_TOKEN'))){
                        console.debuglog("Sucesso no upload da imagem");
                        var link = '[SERVER]/public/'+response.upload.file;
                        console.debuglog("url gerada: "+link);
                        $('<img class="preload" data-url="'+link+'" data-type="photo" data-hash="'+response.upload.hash+'" />').attr("src", $('#inputFilePost').data('uri') ).load(onLoadImg);
                        $('#imgToPost').css('background-image', '');
                        $('#inputFilePost').val("");
                        $('#inputFilePost').data('uri', '');
                        $('#labelTargetPost').removeClass('dropped');
                        //$('<img class="preload" data-url="'+link+'" />').attr("src", addressServer+'/public/'+response.upload).load(onLoadImg);
                    }else{
                        console.debuglog("Falha no upload da imagem");
                        if(response.ACCESS==undefined || response.ACCESS!=='INVALID_TOKEN'){
                            lookConnections=true;
                            showAlert('nFailure', i18n.t("alertas.falhaAutentificacaoPart1")+window.localStorage.getItem('email')+i18n.t("alertas.falhaAutentificacaoPart2"));
                        }else{
                            showAlert('nFailure', i18n.t("alertas.falhaConexao"), 5);
                        }
                    }
                }
            });
            $('#sendFilePost').submit();
        }
    }
}

var ocultaBotoesPost = function(){
    $('.ui-dialog-buttonpane').each(function(idx, elm){
        if($(elm).parent().find($('#dialogPost')).length>0){
            $(elm).hide();
        }
    });
}

var mostraBotoesPost = function(){
    $('.ui-dialog-buttonpane').each(function(idx, elm){
        if($(elm).parent().find($('#dialogPost')).length>0){
            $(elm).show();
        }
    });
}

var ocultaBotoesContact = function(){
    $('.ui-dialog-buttonpane').each(function(idx, elm){
        if($(elm).parent().find($('#dialogContact')).length>0){
            $(elm).hide();
        }
    });
}

var mostraBotoesContact = function(){
    $('.ui-dialog-buttonpane').each(function(idx, elm){
        if($(elm).parent().find($('#dialogContact')).length>0){
            $(elm).show();
        }
    });
}


var validePostsToSend = function(){
    console.debuglog("Função validePostsToSend");
    var listaShare = "";
    $( "#toShare option:selected" ).each(function() {
        listaShare+= ($( this ).val()+"|");
    });
    console.debuglog("Compartilhando postagem com os usuários: "+listaShare);
    if(listaShare!==""){
        showLoader('loaderPost');
        ocultaBotoesPost();
        sendPostAjax(listaShare);
    }else{
        console.debuglog("Detectou tentativa de compartilhamento de postagem para nenhum usuário");
        showAlert('nWarning', i18n.t("alertas.selecioneUmaOpcao"));
    }
}


var valideContactToSave = function(){
    console.debuglog("Função do evento click do botão salvar contato");
    ocultaBotoesContact();
    if(isDeviceMobile()){
        console.debuglog("Salvando contato");
        showLoader('loaderContact');
        var name = $("#newContactName").val();
        var email = $("#newContactEmail").val();
        // create a new contact object
        var contact = navigator.contacts.create();
        contact.displayName = name;
        contact.nickname = name;            // specify both to support all devices

        var emails = [];
        emails[0] = new ContactField("home", email, false);
        contact.emails = emails;

        // populate some fields
        var nameContact = new ContactName();
        nameContact.givenName = name;
        contact.name = nameContact;

        // save to device
        contact.save(function(contact) {
            console.debuglog("Sucesso ao salvar contato");
            showAlert('nSuccess', i18n.t("alertas.sucessoSalvarContato"), 3);
            $('a[data-usr="'+email+'"]').each(function() {
                $(this).text(name);
                $(this).removeClass('green');
            });
            hideLoader('loaderContact');
            mostraBotoesContact();
            closeContactDialog();
            //Busca os contatos novamente
            console.debuglog("Chamando o listar contatos, para pegar o novo contato");
            telephoneNumber.getListContacts(function(resultContacts) {
                console.debuglog("Sucesso ao chamar listagem de contatos do dispositivo");
                saveContacts(resultContacts, carregaListaContatos);
            }, onErrorPlugin);
        }, function(contactError) {
            console.debuglog("Falha ao salvar contato");
            showAlert('nFailure', i18n.t("alertas.falhaSalvarContato"), 5);
            mostraBotoesContact();
        });
    }else{
        console.debuglog("Detectou que não se trata de dispositivo movel, entao apenas fecha o dialog e adiciona o contato na lista temporaria");
        var name = $("#newContactName").val();
        var email = $("#newContactEmail").val();
        showAlert('nSuccess', i18n.t("alertas.sucessoSalvarContato"), 3);
        $('a[data-usr="'+email+'"]').each(function() {
            $(this).text(name);
            $(this).removeClass('green');
        });
        listFriendsInApp.push([name, email]);
        listAllFriends.push(email);
        var group = $($('#toShare optgroup').get(1));
        if($('#toShare optgroup').get(1)){
            group.append($("<option></option>").attr("value",email).text(name));
        }else{
            group = $("<optgroup data-i18n='[label]criarPostagem.amigosDoGhost'></optgroup>");
            group.append($("<option></option>").attr("value",email).text(name)); 
            $('#toShare').append(group);
        }
        $("#toShare").trigger("liszt:updated");
        mostraBotoesContact();
        closeContactDialog();
    }
}


var sendPostAjax = function(listaShare){
    console.debuglog("Função sendPostAjax");
    var listResources = [];
    $("img[class='preload']").each(function(){
        var img = $(this).data('url');
        var youtubeUrl = img.match(youtubeRegex);
        var vimeoUrl = img.match(vimeoRegex);
        if(youtubeUrl || vimeoUrl || $(this).data("type")==="video"){
            typeRes = 'video';
            listResources.push({source: img, type: typeRes});
        }else{
            typeRes = 'photo';
            listResources.push({source: img, type: typeRes});
        }
    });
    
    dados={ idextern:'null', resources: JSON.stringify(listResources), message: $("#newPostText").val(), idchannel: 'friends', userlist: listaShare };

    ajaxNodeJs(addressServer, 'compartilharpost', 'POST', null, function (result){
        if(result!=undefined && result!=null && result.post!=undefined){
            ultimoPostEnviado=result.post._id;
            if(result.post.status==="1" || result.post.status===1){
                showAlert('nSuccess', i18n.t("alertas.postagemGeradaP3"), 3);
            }else{
                showAlert('nSuccess', i18n.t("alertas.postagemGeradaP1")+(parseInt(result.post.status)-1)+i18n.t("alertas.postagemGeradaP2"), 3);
            }
            closePostDialog();
            syncPosts();
        }else{
            showAlert('nFailure', i18n.t("alertas.falhaCompartilharPost"), 5);
        }
        hideLoader('loaderPost');
        mostraBotoesPost();
    }, function (response){ 
        showAlert('nFailure', i18n.t("alertas.falhaCompartilharPost"), 5);
        mostraBotoesPost();
    }, dados);
}


var closePostDialog = function(){
    console.debuglog("Função closePostDialog");
    $("#btnSharePost").show();
    $("#newPostText").val("");
    $('#newPostText').change();
    $("#newPostURL_IMG").val("");
    $("#preloadPost").empty();
    $('#imgToPost').css('background-image', '');
    $('#inputFilePost').val("");
    $('#inputFilePost').data('uri', '');
    $('#labelTargetPost').removeClass('dropped');
    $('#toPostIMGUpload').hide();
    $('#toPostIMGLinkPreload').show();
    $('#btSelectSrcImage').data('i18n', 'botao.link');
    $('#btSelectSrcImage').text(i18n.t('botao.link'));
    $('#btSelectSrcImageIcon').removeClass();
    $('#btSelectSrcImageIcon').addClass('icos-link');
    $('#progressBarPost').hide();
    $('#progressBarPost').css({width: "0%"});
    $('#dialogPost').dialog( "close" );
}

var closeContactDialog = function(){
    console.debuglog("Função closeContactDialog");
    $("#newContactName").val("");
    $("#newContactEmail").val("");
    $('#dialogContact').dialog( "close" );
}


function uploadPhoto(imageURI, inputDiv, imgDiv) {
    console.debuglog("Função uploadPhoto");
    console.debuglog("imageURI: "+imageURI);
    $('#'+imgDiv).css('background-image', 'url(' + imageURI + ')');
    $('#'+inputDiv).data("uri", imageURI);
    $('#'+inputDiv).change();
}

function onFailCamera(message) {
    console.debuglog("Função onFailCamera");
    alert(message);
}

function carregarPerfil(){
    console.debuglog("Função carregarPerfil");
    var fnome = window.localStorage.getItem('fnome');
    loadPicturePerfil(window.localStorage.getItem('email'), 'userPictureFace');
    
    $('#inputUserIDSend').val($.base64.btoa(window.localStorage.getItem('email')));
    $('#inputSerialDeviceSend').val($.base64.btoa(window.localStorage.getItem('serialdevice')));
    $('#inputBuildSend').val($.base64.btoa(BUILD_NUMBER));
    $('#inputUserIDPost').val($.base64.btoa(window.localStorage.getItem('email')));
    $('#inputSerialDevicePost').val($.base64.btoa(window.localStorage.getItem('serialdevice')));
    $('#inputBuildPost').val($.base64.btoa(BUILD_NUMBER));

    var $droptargetsend = $('#labelTargetSend'),
        $droptargetpost = $('#labelTargetPost'),
        $dropinputsend  = $('#inputFileSend'),
        $dropinputpost  = $('#inputFilePost'),
        $dropimgsend    = $('#imgToSend'),
        $dropimgpost    = $('#imgToPost'),
        $removersend    = $('[data-action="remove_current_image_send"]');

    $dropinputsend.change(function(e) {
        console.debuglog("Função do evento change do inputFileSend");
        $droptargetsend.addClass('dropped');
        $removersend.removeClass('disabled');
        if($dropinputsend.get(0).files.length > 0){
            var file = $dropinputsend.get(0).files[0], reader = new FileReader();
            reader.onload = function(event) {
                $dropimgsend.css('background-image', 'url(' + event.target.result + ')');
                $dropinputsend.data('uri', event.target.result);
            }
            reader.readAsDataURL(file);
        }
    });

    $dropinputpost.change(function(e) {
        console.debuglog("Função do evento change do inputFilePost");
        $droptargetpost.addClass('dropped');
        if($dropinputpost.get(0).files.length > 0){
            var file = $dropinputpost.get(0).files[0], reader = new FileReader();
            reader.onload = function(event) {
                $dropimgpost.css('background-image', 'url(' + event.target.result + ')');
                $dropinputpost.data('uri', event.target.result);
            }
            reader.readAsDataURL(file);
        }
    });

    $removersend.on('click', function() {
        console.debuglog("Função do evento click do removersend");
        $dropimgsend.css('background-image', '');
        $droptargetsend.removeClass('dropped');
        $removersend.addClass('disabled');
        $('#inputFileSend').val('');
    });

    $('#sendFileSend').attr('action', addressServer+"/upload");
    $('#sendFilePost').attr('action', addressServer+"/uploadpost");

    if(fnome==='undefined'){
        fnome="Usuario";
    }
    $('#userNameFace').html(fnome);
}


function ajustaDivConfig(){
    console.debuglog("Função ajustaDivConfig");
    // Bind to the resize event of the window object
    $(window).on("resize", function () {
        console.debuglog("Resize na tela para: "+ window.innerHeight +"x"+window.innerWidth);
        //(max-width: 480px) and (min-width: 320px)
        if((window.innerHeight>=320) && (window.innerWidth<=480)){
            $('.secNav .secWrapper').height( window.innerHeight - ($('#top').height()+94)); // + altura do mainNav
        }else{
            $('.secNav .secWrapper').height( window.innerHeight - ($('#top').height()));
        }
        if($("iframe[data-type='video']").size()>0){
            console.debuglog("Encontrou videos na timeline, chamando resize de cada iframe");
            $("iframe[data-type='video']").each(function(){
                var newWidthScreen = $(this).parent().parent().parent().parent().parent().parent().width();
                var porporcionalAltura = calculaAlturaProporcional(newWidthScreen);
                this.width = newWidthScreen-24; //-24 da margem direita
                this.height = porporcionalAltura;
                console.debuglog("Novo tamanho do iframe: "+this.height+"x"+this.width);
            });
        }
    // Invoke the resize event immediately
    }).resize();
}

function getPost(callback, idpost, translate){
    console.debuglog("Função getPost");
    var langToTrans = (translate?i18n.lng():'none');
    console.debuglog("Tradução para: "+langToTrans);
    dados = { 'info_idpost': idpost, 'translate': langToTrans };
    ajaxNodeJs(addressServer, 'getpost', 'GET', null, callback, function (response){}, dados);
}

function removePostComment(idPost, idComment){
    console.debuglog("Função removePostComment");
    dados = { info_idpost: idPost, info_idcomment: idComment };
    ajaxNodeJs(addressServer, 'removecomment', 'GET', null, afterSendComment, function (response){ console.debuglog(response); }, dados);
}

function sendPostComment(idPost, msg){
    console.debuglog("Função sendPostComment");
    var email = window.localStorage.getItem('email');
    dados = { iduser: email, nameuser: window.localStorage.getItem('fullname'), idpost: idPost, message: msg};
    ajaxNodeJs(addressServer, 'newcomment', 'POST', 'loader', afterSendComment ,function (response){ console.debuglog(response); }, dados);
}

var addNewContact = function(contact){
    console.debuglog("Função addNewContact");
    if($(contact).hasClass( "green" )){
        var name = $(contact).text();
        if(name.indexOf("+")===0){
            name=name.substring(2);
        }
        $("#newContactName").val(name);
        $("#newContactEmail").val($(contact).data('usr'));
        console.debuglog("Abrindo dialog de novo contato com os valores: '"+name+"', '"+$(contact).data('usr')+"'");
        hideLoader('loaderContact');
        $('#dialogContact').dialog('open');
    }
}

function carregaListaContatos(){
    console.debuglog("Função carregaListaContatos");
    ajaxNodeJs(addressServer, 'listfriends', 'POST', null, function (resultContacts){
        console.debuglog("Retornou a lista de contatos:");
        console.debuglog(resultContacts.response);
        listFriendsInApp=[];
        listFriendsOutApp=[];
        listAllFriends=[];
        $("#toShare").find('option').remove();
        $("#toShare").find('optgroup').remove();
        if(resultContacts.response!=undefined && resultContacts.response!=null && (resultContacts.response.comapp!=undefined || resultContacts.response.comapp!=semapp)){
            var comapp = resultContacts.response.comapp;
            var semapp = resultContacts.response.semapp;
            if(comapp!=undefined){
                console.debuglog("Lista de contatos contém "+comapp.length+" contatos com ghost instalado");
                for (var i = 0; i < comapp.length; i++) {
                    if(listAllFriends.indexOf(comapp[i][1])===-1 && comapp[i][1]!==window.localStorage.getItem('email')){
                        listFriendsInApp.push([comapp[i][0], comapp[i][1]]);
                        listAllFriends.push(comapp[i][1]);
                    }
                }
            }
            if(semapp!=undefined){
                console.debuglog("Lista de contatos contém "+semapp.length+" contatos sem ghost instalado");
                for (var i = 0; i < semapp.length; i++) {
                    if(listAllFriends.indexOf(semapp[i][1])===-1 && semapp[i][1]!==window.localStorage.getItem('email')){
                        listFriendsOutApp.push([semapp[i][0], semapp[i][1]]);
                        listAllFriends.push(semapp[i][1]);
                    }
                }
            }
            var cleanOption = $("<option value=''></option>");
            var newGroupAll = $("<optgroup data-i18n='[label]criarPostagem.todosLabel'></optgroup>");
            newGroupAll.append($("<option value='TODOS1' data-i18n='criarPostagem.todosGhost'></option>"));
            newGroupAll.append($("<option value='SELF' data-i18n='criarPostagem.somenteParaMim'></option>"));
            var newGroupC = $("<optgroup data-i18n='[label]criarPostagem.amigosDoGhost'></optgroup>");
            $.each(listFriendsInApp, function(key, value) {
                newGroupC.append($("<option></option>").attr("value",value[1]).text(value[0])); 
            });
            //var newGroupS = $("<optgroup data-i18n='[label]criarPostagem.amigosSemGhost'></optgroup>");
            //$.each(listFriendsOutApp, function(key, value) {
            //    newGroupS.append($("<option></option>").attr("value",value[1]).text(value[0])); 
            //});
            $('#toShare').append(cleanOption);
            $('#toShare').append(newGroupAll);
            $('#toShare').append(newGroupC);
            //$('#toShare').append(newGroupS);
        }
        $("#toShare_chzn").attr("style", "width: 100%!important;");
        $("#toShare").trigger("liszt:updated");
        setDefaultLanguage();
        $("#toShare").trigger("liszt:updated");
        hideLoader('loader');
        if(window.localStorage.getItem('fnome')!=="undefined" && window.localStorage.getItem('fnome')!==""){
            lookConnections=false;
            loadLastPosts(false);
        }
    }, function (response){ 
        console.debuglog(response);
        hideLoader('loader');
    }, null);
}

var setLanguage = function(lang){
    console.debuglog("Função setLanguage");
    console.debuglog("Setando linguagem: "+lang);
    i18n.setLng(lang, {fixLng: true}, function(translation){ 
        $('[data-i18n]').i18n(); 
        $("#pais").trigger("liszt:updated");
        $("#toShare").trigger("liszt:updated");
        $('#dialogPost').dialog("refresh");
    });
}

var setDefaultLanguage = function(){
    console.debuglog("Função setDefaultLanguage");
    if(navigator.language==='pt-BR'){
        setLanguage(navigator.language);
    }else{
        setLanguage('en-US');
    }
}

function getEmbeddedPlayer(urlV, height, width){
    console.debuglog("Função getEmbeddedPlayer");
    var youtubeUrl = urlV.match(youtubeRegex);
    var vimeoUrl = urlV.match(vimeoRegex);
    if( youtubeUrl ){
        console.debuglog("Detectou video do youtube");
        if(urlV.indexOf('/attribution_link?u=/')>-1){
            urlV=urlV.substring(0, urlV.indexOf('/attribution_link?u=/'))+urlV.substring(urlV.indexOf('/attribution_link?u=/')+20);
            youtubeUrl = urlV.match(youtubeRegex);
        }
        var v = youtubeUrl[1];
        //A alteracao destas divs implica na modificacao da funcao ajustaDivConfig, onde usa o .parent() para pegar os tamanhos das divs acima do iframe
        return '<div class="post_video"><iframe src="http://www.youtube.com/embed/'+v+'?rel=0" data-type="video" height="'+height+'" width="'+width+'" allowfullscreen="" frameborder="0"></iframe></div>';
    }else if( vimeoUrl ){
        console.debuglog("Detectou video do vimeo");
        return '<div class="post_video"><iframe src="http://player.vimeo.com/video/'+vimeoUrl[3]+'" data-type="video" width="'+width+'" height="'+height+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
    }else{
        console.debuglog("Detectou outro formato de video");
        return '<video class="post_video" controls poster="'+urlV+'-thumb.png'+'" height="'+height+'" width="'+width+'">\
              <source src="'+urlV+'" type="video/mp4">\
                [<a href="'+urlV+'" target="_blank" data-i18n="posts.linkVideo"></a>]\
            </video>';
    }
}

var findUrls = function( text ){
    console.debuglog("Função findUrls");
    var source = (text || '').toString();
    var urlArray = [];
    var url;
    var matchArray;
    // Iterate through any URLs in the text.
    while( (matchArray = urlRegex.exec( source )) !== null ){
        var token = matchArray[0];
        urlArray.push( token );
    }
    console.debuglog("Encontrou urls: ");
    console.debuglog(urlArray);
    return urlArray;
}

var openInBrowser = function(tagA){
    console.debuglog("Função openInBrowser");
    window.open($(tagA).data('url'), "_system");
}

var urlifyOnlyHiper = function(text) {
    console.debuglog("Função urlifyOnlyHiper");
    return text.replace(urlRegex, function(url) {
        return '<a data-url="' + url + '" onclick="openInBrowser(this);" target="_blank">'+url+'</a>';
    })
    // or alternatively
    // return text.replace(urlRegex, '<a href="$1">$1</a>')
}

var updateTimePosts = function(){
    console.debuglog("Função updateTimePosts");
    $('span.time').each(function () {
        var self = $(this);
        self.html(elapsedTime($(this).data("time")));
    });
}

var toggleDiv = function(div){
    console.debuglog("Função toggleDiv");
    if($('#'+div.trim()).is(':visible')){
        $('#'+div.trim()).hide();
    }else{
        $('#'+div.trim()).show();
    }
}

var plusCommentsLoad = function(idPost){
    console.debuglog("Função plusCommentsLoad");
    var after = $(('#comments_plus_'+idPost).trim()).attr('data-after');
    getPostComments(function(response){
        console.debuglog("Chamando montagem de comentários para:");
        console.debuglog(response);
        montaComments(response, idPost);
    }, idPost, after);
}

var updatePosts = function(posts, idx){
    console.debuglog("Função updatePosts");
    getPost(function (response){
        console.debuglog("Função sucesso de getPost");
        var next = idx+1;
        if(next<posts.length){
            updatePosts(posts, next);
        }
        atualizaPost(response);
    }, posts[idx]);
}

function escapeRegExp(string) {
    console.debuglog("Função escapeRegExp");
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(string, find, replace) {
    console.debuglog("Função replaceAll");
    return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

var loadLastPosts = function(onlyOld){
    console.debuglog("Função loadLastPosts");
    //showLoader('loader');
    listLastPosts(onlyOld); //tambem carrega os novos
    //hideLoader('loader');
}

var removeIMGPost = function(idimg){
    console.debuglog("Função removeIMGPost");
    var hashres = $('#liIMG_'+idimg).data('hash');
    sendRequestRemoveFiles(hashres);
    $('#liIMG_'+idimg).remove();
}

var sendRequestRemoveFiles = function(hashres){
    console.debuglog("Excluindo recurso no servidor, hash: "+hashres);
    ajaxNodeJs(addressServer, 'removeprepost', 'GET', null, function(resultado, loader){
        if(resultado!=null && resultado!=undefined){
            if(resultado.response==="OK"){
                console.debuglog("Recurso excluido com sucesso no servidor");
            }else{
                console.debuglog("Falha ao excluir recurso no servidor");
            }
        }else console.debuglog("Falha ao excluir recurso no servidor");
    }, function(erro){
        console.debuglog(JSON.stringify(erro));
    }, {hash: hashres});
}

var removeAllFilesPost = function(){
    $('li [data-hash]').each(function(idx, item){
        sendRequestRemoveFiles($(item).attr('data-hash'));
    });
}

function listLastPosts(onlyOld){
    console.debuglog("Função listLastPosts");
    if(!lookConnections && !$('#chSilenciar').is(':checked')){
        var lastsync = null;
        if( listPosts.length == 0 ){
            console.debuglog("Nenhuma postagem na timeline, setando varialvel como onlyOld = false");
            onlyOld = false;
        }
        if(onlyOld){
            console.debuglog("variavel onlyOld=true");
            showLoader('loaderInferior');
            lastsync = $(('#content_posts div.widget:last-child').trim()).attr('data-time');
            console.debuglog("variavel lastsync='"+lastsync+"'");
        }
        dados = { onlyoldposts : onlyOld, datelastsync: lastsync, limit:globalConfigs['posts_for_refresh']};
        console.debuglog("Buscando postagens mais velhas no servidor");
        ajaxNodeJs(addressServer, 'lastposts', 'GET', null, function(resultado, loader){
            console.debuglog("Sucesso ao buscar postagens mais antigas no servidor");
            if(resultado!=undefined && resultado!=null && resultado.posts.length>0){
                //confirmar sincronismo
                var confirmar=[];
                for (var i = 0; i < resultado.posts.length; i++) {
                    if(resultado.posts[i].synchronized === 0)
                        confirmar.push(resultado.posts[i]._id);
                }
                var recebidos=[];
                for (var i = 0; i < resultado.posts.length; i++) {
                    recebidos.push(resultado.posts[i]._id);
                }
                if(confirmar.length>0){
                    console.debuglog("Confirmando recebimento para os posts:");
                    console.debuglog(confirmar);
                    ajaxNodeJs(addressServer, 'postsConfirm', 'GET', null, function(res){
                        console.debuglog("Sucesso na confirmação de recebimento dos posts");
                    }, function(erro){ console.debuglog(JSON.stringify(erro)); }, {postsRec: confirmar});
                }
                for(var i=0;i<resultado.posts.length;i++){
                    if( $.inArray( resultado.posts[i]._id, listPosts ) == -1 ){
                        var sharedBy = undefined;
                        if((resultado.posts[i].iduser_src!=undefined && resultado.posts[i].iduser_src.length>1)){
                            sharedBy = resultado.posts[i].iduser_src[1] || resultado.posts[i].iduser_src[0];
                        }
                        createBoxPost(resultado.posts[i]._id, resultado.posts[i].created_time, 'down', sharedBy);
                    }
                }
                console.debuglog("Chamando updatePosts para os posts: ");
                console.debuglog(recebidos);
                updatePosts(recebidos, 0);
            }
            hideLoader('loaderInferior');
        }, function(erro){
            console.debuglog("Falha ao buscar postagens mais antigas no servidor");
            hideLoader('loaderInferior');
            console.debuglog(JSON.stringify(erro));
        }, dados);
    }
}

var syncPosts = function(){
    console.debuglog("Função syncPosts");
    if(!lookConnections && !$('#chSilenciar').is(':checked')){
        dados = { limit:globalConfigs['posts_for_refresh']};
        console.debuglog("Buscando novos posts no servidor");
        ajaxNodeJs(addressServer, 'newposts', 'GET', null, function(resultado, loader){
            console.debuglog("Retornou "+resultado.posts.length+" novos posts");
            if(resultado.posts.length>0){
                //confirmar sincronismo
                var recebidos=[];
                var lastPostRec='';
                for (var i = 0; i < resultado.posts.length; i++) {
                    recebidos.push(resultado.posts[i]._id);
                    lastPostRec=resultado.posts[i]._id;
                }
                ajaxNodeJs(addressServer, 'postsConfirm', 'GET', null, function(res){
                    console.debuglog("Sucesso na confirmação de recebimento dos posts com a resposta: "+res.response);
                    if(res.response==='OK'){
                        if(resultado!=undefined && resultado!=null && resultado.posts.length>0){
                            for(var i=0;i<resultado.posts.length;i++){
                                if( $.inArray( resultado.posts[i]._id, listPosts ) == -1 ){
                                    var sharedBy = undefined;
                                    if((resultado.posts[i].iduser_src!=undefined && resultado.posts[i].iduser_src.length>1)){
                                        sharedBy = resultado.posts[i].iduser_src[1] || resultado.posts[i].iduser_src[0];
                                    }
                                    createBoxPost(resultado.posts[i]._id, resultado.posts[i].created_time, 'top', sharedBy);
                                }else{
                                    console.debuglog("Post "+resultado.posts[i]._id+" já existe na timeline");
                                }
                            }
                            updatePosts(recebidos, 0);
                            if($.inArray(ultimoPostEnviado, recebidos)===-1 && !onMinimize && ($(window).scrollTop()>1000)){
                                $('#divtopalert').click(function(){
                                    scrollToAnchor('post_channelName_'+lastPostRec);
                                    $('.topAlert').hide();
                                });
                                $('#divtopalert').fadeToggle(150);
                            }
                            if(onMinimize && $.inArray(ultimoPostEnviado, recebidos)===-1){
                                window.plugin.notification.local.add({ 
                                    message: (i18n.t('alertas.novaPostagem')), 
                                    icon: 'ic_launcher', smallIcon: 'ic_launcher', 
                                    json: JSON.stringify({ idpost: lastPostRec }),
                                    ongoing: false,
                                    autoCancel: true
                                });
                            }
                            ultimoPostEnviado="";
                        }
                    }
                }, function(erro){
                    console.debuglog("Falha na confirmação de recebimento dos posts");
                    console.debuglog(JSON.stringify(erro));
                }, {postsRec: recebidos});
            }
        }, function(erro){
            console.debuglog("Falha ao buscar novos posts no servidor");
            console.debuglog(JSON.stringify(erro));
        }, dados);
    }
}

var simulaClickFilePost = function(){
    console.debuglog("Função simulaClickFilePost");
    $('#imgToPost').click();
}

var simulaClickFileSend = function(){
    console.debuglog("Função simulaClickFileSend");
    $('#imgToSend').click();
}

var afterSendComment = function(response, loader){
    if (loader) hideLoader(loader);
    console.debuglog("Função afterSendComment");
    $(('#send_comment_text_'+response.post._id).trim()).val("");
    $('#send_comment_text_'+response.post._id).change();
    $('.auto').elastic();
    $('.auto').trigger('update');
    console.debuglog("Chamando atualização do post: "+response.post._id);
    updatePost(response.post._id);
    ultimoPostEnviado=response.post._id;
    //ajaxNodeJs(addressServer, 'postsConfirm', 'GET', null, function(res){}, function(erro){}, {postsRec: [idPost.post] });
}

var afterRemoveComment = function(idPost){
    console.debuglog("Função afterRemoveComment");
    console.debuglog("Chamando atualização do post: "+idPost.post);
    updatePost(idPost.post);
}

var sendComment = function(idPost){
    console.debuglog("Função sendComment");
    if($(('#send_comment_text_'+idPost).trim()).val()!==""){
        console.debuglog("Chamando funcao que envia o comentário ao servidor com o texto: "+$(('#send_comment_text_'+idPost).trim()).val());
        sendPostComment(idPost, $(('#send_comment_text_'+idPost).trim()).val());
    }else{
        console.debuglog("Detectou tentativa de envio de comentário em branco");
        showAlert('nWarning', i18n.t("alertas.digiteOComentario"));
    }
}

var removeComment = function(idPost,idComment){
    if(confirm(i18n.t("confirmacoes.removerComentario"))){
        console.debuglog("Função removeComment");
        console.debuglog("Chamando função removePostComment com os parâmetros idPost="+idPost+", idComment="+idComment);
        removePostComment(idPost, idComment);
    }
}

var removePost = function(idPost){
    console.debuglog("Função removePost");
    var idxInArray = $.inArray(idPost, myPosts);
    if( idxInArray >-1 ){
        console.debuglog("Detectou remoção de postagem do próprio usuário");
        if(confirm(i18n.t("confirmacoes.confirmarExclusaoPostagemTodosContatos"))){
            dados={ idpost: idPost };
            console.debuglog("Chamando remoção de postagem no servidor");
            ajaxNodeJs(addressServer, 'removepost', 'POST', null, function (result){
                if(result!=undefined && result!=null){
                    if(result.response.status!=="POST_NOT_EXIST"){
                        console.debuglog("Sucesso na remoção de postagem no servidor");
                        if(result.response.status==="1" || result.response.status===1){
                            showAlert('nSuccess', i18n.t("alertas.postagemRemovidaDeSuaTimeline"), 2);
                        }else{
                            showAlert('nSuccess', i18n.t("alertas.postagemRemovidaP1")+(parseInt(result.response.status)-1)+i18n.t("alertas.postagemRemovidaP2"), 2);
                        }
                    }else{
                        console.debuglog("Postagem já havia sido removida pelo autor");
                        showAlert('nWarning', i18n.t("alertas.postagemRemovida"), 3);
                    }
                    myPosts.splice(idxInArray,1);
                    idxInArray = $.inArray(idPost, listPosts);
                    listPosts.splice(idxInArray,1);
                    $('#post_'+idPost).remove();
                }else{
                    console.debuglog("Falha na remoção de postagem no servidor");
                    showAlert('nFailure', i18n.t("alertas.falhaRemoverPostagem"), 5);
                }
            }, function (response){ 
                console.debuglog("Falha na remoção de postagem no servidor");
                showAlert('nFailure', i18n.t("alertas.falhaRemoverPostagemVerifiqueConexao"), 5);
            }, dados);
        }
    }else{
        console.debuglog("Detectou remoção de postagem de terceiros");
        if(confirm(i18n.t("confirmacoes.removerPostagemSuaTimeLine"))){
            dados={ idpost: idPost };
            console.debuglog("Chamando remoção de postagem no servidor");
            ajaxNodeJs(addressServer, 'removepost', 'POST', null, function (result){ 
                if(result!=undefined && result!=null){
                    if(result.response.status!=="POST_NOT_EXIST"){
                        console.debuglog("Sucesso na remoção de postagem no servidor");
                        showAlert('nSuccess', i18n.t("alertas.postagemRemovidaDeSuaTimeline"), 2);
                    }else{
                        console.debuglog("Postagem já havia sido removida pelo autor");
                        showAlert('nWarning', i18n.t("alertas.postagemRemovida"), 3);
                    }
                    idxInArray = $.inArray(idPost, listPosts);
                    listPosts.splice(idxInArray,1);
                    $('#post_'+idPost).remove();
                }else{
                    console.debuglog("Falha na remoção de postagem no servidor");
                    showAlert('nFailure', i18n.t("alertas.falhaRemoverPostagem"), 5);
                }
            }, function (response){ 
                console.debuglog("Falha na remoção de postagem no servidor");
                showAlert('nFailure', i18n.t("alertas.falhaRemoverPostagemVerifiqueConexao"), 5);
            }, dados);
        }
    }
}

var showCommentsBox = function(idPost){
    console.debuglog("Função showCommentsBox");
    toggleDiv('comments_'+idPost);
    toggleDiv('send_comment_'+idPost);
    //if($('#send_comment_'+idPost).attr('data-scommment') === 'true'){}
    $( "#selectBoxShare" ).hide();
}

var showSelectBoxShare = function(idPost){
    console.debuglog("Função showSelectBoxShare");
    if($( "#selectBoxShare" ).attr("data-post") === idPost){
        console.debuglog("toShare encontrado no post local");
        toggleDiv('selectBoxShare');
        //$( '#comments_'+idPost ).hide();
        //$( '#send_comment_'+idPost ).hide();
    }else{
        console.debuglog("toShare encontrado fora da postagem local");
        $( "#selectBoxShare" ).attr("data-post", idPost);
        $( "#selectBoxShare" ).appendTo( "#boxShare_"+idPost );
        $( "#selectBoxShare" ).show();
        $( "#btnSharePost" ).show();
    }
}

var sharePost = function(){
    console.debuglog("Função sharePost");
    var idPost = $( "#selectBoxShare" ).attr("data-post");
    var listaShare = "";
    $( "#toShare option:selected" ).each(function() {
        listaShare+= ($( this ).val()+"|");
    });
    console.debuglog("Compartilhando com os usuários: "+listaShare);
    if(listaShare!==""){
        dados={ idpost: idPost, idchannel: 'friends', userlist: listaShare, nameuser: window.localStorage.getItem('fullname') };
        console.debuglog("Chamando compartilhamento no servidor");
        ajaxNodeJs(addressServer, 'compartilharpost', 'POST', 'loader', function (result, loader){
            hideLoader(loader);
            if(result!=undefined && result!=null && result.post.status!=='POST_NOT_EXIST'){
                console.debuglog("Susseso no compartilhamento no servidor");
                showAlert('nSuccess', i18n.t("alertas.postagemGeradaP1")+result.post.status+i18n.t("alertas.postagemGeradaP2"), 2);
                showSelectBoxShare(idPost);
            }else{
                console.debuglog("Falha no compartilhamento no servidor, postagem já removida");
                showAlert('nFailure', i18n.t("alertas.postagemRemovida"), 10);
            }
        }, function (response){ 
            console.debuglog("Falha no compartilhamento no servidor");
            showAlert('nFailure', i18n.t("alertas.falhaCompartilharPost"), 10);
        }, dados);
    }else{
        console.debuglog("Detectou lista de usuários vazia");
        showAlert('nWarning', i18n.t("alertas.selecioneUmaOpcao"));
    }
}

var tranlatePost = function(evt){ 
    console.debuglog("Função tranlatePost");
    console.debuglog("Traduzindo e atualizando postagem: "+$(evt.target).data('idpost'));
    updatePost($(evt.target).data('idpost')); 
}

function calculaAlturaProporcional(newWidthScreen){
    console.debuglog("Função calculaAlturaProporcional");
    var heightVideo = 315, widthVideo=newWidthScreen;
    for (var i = 0; i < sizesVideo.length; i++) {
        if(sizesVideo[i][0] <= widthVideo){
            widthVideo=sizesVideo[i][0];
            heightVideo=sizesVideo[i][1];
            break;
        }
    }
    var porporcionalLargura = widthVideo / newWidthScreen;
    var porporcionalAltura = heightVideo / porporcionalLargura;
    console.debuglog("Calculo de proporcional encontrou o tamanho: "+porporcionalAltura+"x"+porporcionalLargura);
    return porporcionalAltura;
}

var updatePost = function(idPost){
    console.debuglog("Função updatePost");
    var translate = $('#chTranslate_'+idPost).is(':checked');
    console.debuglog("translate: "+translate);
    getPost(atualizaPost, idPost, translate);
}

var atualizaPost = function(response){
    console.debuglog("Função atualizaPost");
    var idPost = response.post._id;
    if(response!=null && response!= undefined && response.post.status!=="POST_NOT_EXIST"){
        console.debuglog("Atualizando postagem: "+idPost);
        if(response.post.from.username===window.localStorage.getItem('email')){
            if($.inArray(idPost, myPosts)===-1){
                myPosts.push(idPost);
            }
        }
        if(response.post.from.type==="user"){
            $('#post_pic_channel_'+idPost).attr('src','images/user_logo.png');
            loadPicturePerfil(response.post.from.username, 'post_pic_channel_'+idPost);
        }
        $('#post_title_widget_'+idPost).html(response.post.from.name).removeAttr('data-i18n');
        var $titulo = $('#post_channelName_'+idPost);
        $titulo.on('click', function(){
            addNewContact(this);
        });
        if(($.inArray(response.post.from.username, listAllFriends)===-1) && (response.post.from.username!=="desconhecido") && response.post.from.username!==window.localStorage.getItem('email')){
            console.debuglog("Postagem de um desconhecido");
            $titulo.html("+&nbsp"+response.post.from.name.substring(0,15));
            $titulo.removeAttr('data-i18n');
            $titulo.attr('data-usr', response.post.from.username);
            $titulo.addClass('green');
        }else{
            console.debuglog("Postagem de um conhecido");
            $titulo.html(response.post.from.name.substring(0,15));
            $titulo.removeAttr('data-i18n');
            $titulo.attr('data-usr', response.post.from.username);
        }
        
        if(response.post.from.description==='friends'){
            $('#post_channelCat_'+idPost).html(i18n.t('posts.postDeUsuario')).removeAttr('data-i18n');
        }else{
            $('#post_channelCat_'+idPost).html(response.post.from.description).removeAttr('data-i18n');
        }
        $('#post_timeCreated_'+idPost).html(elapsedTime(response.post.created_time));
        var msg = '<span class="aro"></span>';
        var textoMsg = replaceAll(urlifyOnlyHiper(response.post.message), '\n', '<br />');
        msg+= '<div>'+textoMsg+'</div><br/>';
        msg+= '<div class="myPic postContent" id="contentImagesVideosPost_'+idPost+'"></div>';
        
        $('#post_message_'+idPost).html(msg);
        $('#comments_block_'+idPost).empty();
        $('#comments_block_'+idPost).append('<li class="divider"><span></span></li>');
        $('#send_comment_'+idPost).attr('data-scommment', 'true');
        $(('#comments_block_'+idPost).trim()).empty();
        if(response.post.comments!=undefined){
            montaComments(response.post.comments, idPost);
        }
        
        var resources = response.post.resources;
        if(resources && resources.length>0){
            for (var i = 0; i < resources.length; i++) {
                if ((resources[i].type==='video' && globalConfigs['posts_video']) || (resources[i].type==='photo' && globalConfigs['posts_images'])){
                    var source = resources[i].source;
                    var inServer= false;
                    if(source.indexOf("[SERVER]")>-1){
                        source = addressServer+source.substring(8);
                        inServer = true;
                    }
                    source = $(findUrls(source)).get(0) || "";           
                    if (source && source!==""){    
                        if(resources[i].type==='video'){
                            var newWidthScreen = $('#post_message_'+idPost).width();
                            var porporcionalAltura = calculaAlturaProporcional(newWidthScreen);
                            $('#contentImagesVideosPost_'+idPost).append( getEmbeddedPlayer(source, porporcionalAltura, newWidthScreen) );
                        }else if(resources[i].type==='photo'){
                            var img = $('<img />').attr('src', inServer?source+'-small':source).error(function(evt){
                                $(evt.target).attr('src', "images/elements/loaders/loading.jpeg");
                            });
                            var divGallery = $('<div class="image-galery"></div>');
                            var tx = urlifyOnlyHiper(response.post.message).substring(0, 150);
                            var aGallery = $('<a class="group" title="'+(tx.length==150?(tx+'...'):tx)+'" rel="group1" href="'+source+'"></a>');
                            $(aGallery).append(img);
                            $(divGallery).append(aGallery);
                            $('#contentImagesVideosPost_'+idPost).append(divGallery);
                        }   
                    }
                }
            }
        }

        setDefaultLanguage();
        refreshEventsImageGallery();
    }else{
        console.debuglog("Postagem: "+idPost+" já foi apagada");
        showAlert('nWarning', i18n.t("alertas.postagemRemovida"), 3);
        $('#post_'+idPost).remove();
    }
}

var montaComments = function(comments, idPost){
    console.debuglog("Função montaComments");
    var email = window.localStorage.getItem('email');
    var commentsTep = '';
    var ultimoUserComment = '';
    var sentido = "by_user";
    var li = $(('#comments_block_'+idPost+' li:last-child').trim());
    if ($(li).attr('data-iduser')!== undefined){
        ultimoUserComment = $(li).attr('data-iduser');
        if($(li).hasClass('by_me')){
            sentido = "by_me";
        }else{
            sentido = "by_user";
        }
    }
    console.debuglog("Quantidade de comentários: "+comments.length+" para o post "+idPost);
    for($i=0; $i < comments.length; $i++){
        if(ultimoUserComment !== comments[$i].iduser){
            if(sentido==="by_user"){
                sentido = "by_me";
            }else{
                sentido = "by_user";
            }
        }
        var nomeUser = '';
        if(($.inArray(comments[$i].iduser, listAllFriends)===-1) && comments[$i].iduser!==window.localStorage.getItem('email')){
            nomeUser = '<a data-usr="'+comments[$i].iduser+'" class="green" onclick="addNewContact(this);">'+'+&nbsp'+comments[$i].nameuser+'</a>';
        }else{
            nomeUser = '<a data-usr="'+comments[$i].iduser+'">'+comments[$i].nameuser+'</a>';
        }
        commentsTep+= '<li class="'+sentido+'" data-iduser="'+comments[$i].iduser+'">\
                    <a title=""><img src="images/user_logo.png" alt="" id="imgcomment_'+comments[$i].id+'" /></a>'+
                    ((comments[$i].iduser===email)?'<a onclick="removeComment(\''+idPost+'\',\''+comments[$i].id+'\');" data-i18n="[title]botao.remover"><img src="images/icons/delete_comment.png" alt="" /></a>':'') +
                    '<div class="messageArea">\
                        <span class="aro"></span>\
                        <div class="infoRow">\
                            <span class="name"><strong>'+nomeUser+'</strong><span data-i18n"mensagens.disse"></span></span>\
                            <span class="time" data-time="'+comments[$i].created_time+'">'+elapsedTime(comments[$i].created_time)+'</span>\
                            <div class="clear"></div>\
                        </div>\
                        '+replaceAll(urlifyOnlyHiper(comments[$i].message), '\n', '<br />')+'\
                    </div>\
                    <div class="clear"></div>\
                </li>';
        ultimoUserComment = comments[$i].iduser;
    }
    $(('#comments_plus_'+idPost).trim()).hide();
    $(('#comments_block_'+idPost).trim()).append(commentsTep);

    setDefaultLanguage();
    for($i=0; $i < comments.length; $i++){
        //busca imagens de usuarios:
        loadPicturePerfil(comments[$i].iduser,'imgcomment_'+comments[$i].id);
    }
}

var createBoxPost = function(idPost, created_time, localPush, sharedBy){
    console.debuglog("Função createBoxPost");
    if( $.inArray( idPost, listPosts ) > -1 ){
        console.debuglog("Post "+idPost+" já existe na timeline, removendo...");
        $('#post_'+idPost).remove();
    }
    listPosts.push(idPost);
    var fnome = window.localStorage.getItem('fnome');
    var post = '<!-- Begin Post -->\
        <div class="widget" data-time="'+created_time+'" id="post_'+idPost+'">\
            <div class="whead">'+
                '<div class="on_off"><a class="tablectrl_small bDefault" onclick="removePost(\''+idPost+'\');"><span class="icos-trash"></span></a></div>'+
                (globalConfigs['btn_refresh_post']?'<div class="on_off"><a class="tablectrl_small bDefault" onclick="updatePost(\''+idPost+'\');"><span class="icos-refresh"></span></a></div>': '') +
                '<div class="myInfo">\
                    <div class="myPic postHead"><img id="post_pic_channel_'+idPost+'" src="images/user_logo.png" alt="" /></div>\
                    <h5><a id="post_channelName_'+idPost+'" data-i18n="mensagens.carregando"></a></h5>\
                    <span class="myRole" id="post_channelCat_'+idPost+'"></span>\
                </div>\
                <div class="clear"></div>\
            </div>'+
            (globalConfigs['btn_translate']?'<ul class="niceList params" style="color: #B1B1B1;">\
                <li class="on_off">\
                    <label for="chTranslate_'+idPost+'" data-i18n="translate.botaoTraduzir"></label>\
                    <input type="checkbox" id="chTranslate_'+idPost+'" name="chboxTranslate" data-idpost="'+idPost+'" />\
                    <div class="clear"></div>\
                </li>\
            </ul>':'') +
            '<ul class="niceList params by_user">\
                <li>\
                    <div class="myInfo">\
                        <div class="infoRow">'+
                            ((sharedBy && sharedBy!="null")?'<span class="name" id="post_sharedBy_'+idPost+'">'+sharedBy+' '+i18n.t("mensagens.sharedBy")+'</span>':'')+
                            '<span class="time" data-time="'+created_time+'" id="post_timeCreated_'+idPost+'">'+elapsedTime(created_time)+'</span>\
                        </div>\
                    </div>\
                    <div class="clear"></div>\
                    <div>\
                        <div class="messageArea" id="post_message_'+idPost+'">\
                        </div>\
                        <div class="clear"></div>\
                    </div>\
                </li>\
            </ul>\
            <div>\
                <div class="sendwidget" id="boxShare_'+idPost+'">\
                    <ul class="middleNavR" style="margin-top: 5px!important;padding-bottom: 5px!important;">\
                        <li><a class="tipN" data-i18n="[title]botao.compartilhar" onclick="showSelectBoxShare(\''+idPost+'\');" ><img src="images/icons/middlenav/add.png" alt=""></a></li>\
                        <li><a class="tipN" data-i18n="[title]botao.comentarios" onclick="showCommentsBox(\''+idPost+'\');" ><img src="images/icons/middlenav/dialogs.png" alt=""></a></li>\
                    </ul>\
                </div>\
                <div class="clear"></div>\
            </div>\
            <div id="comments_'+idPost+'" '+(globalConfigs['auto_comment']?'':'style="display: none;"')+'>\
                <ul class="messagesOne" id="comments_block_'+idPost+'">\
                        <li class="divider"><span></span></li>\
                </ul>\
                <div class="postPlusComments" id="comments_plus_'+idPost+'" style="display: none;" data-after="" ><a onclick="plusCommentsLoad(\''+idPost+'\');"><span data-i18n="mensagens.maisComentarios"></span></a></div>\
            </div>\
            <div class="body" id="send_comment_'+idPost+'" '+(globalConfigs['auto_comment']?'':'style="display: none;"')+'>\
                <div class="divider"><span></span></div>\
                <div class="messageTo">\
                    <a title="" class="uName"><img src="'+$('#userPictureFace').attr("src")+'" alt="" /></a><span><strong>'+fnome+'</strong> <span data-i18n="mensagens.envieComentario" </span>\
                </div>\
                <textarea rows="3" cols="" name="textarea" class="auto lim" id="send_comment_text_'+idPost+'" data-i18n="[placeholder]mensagens.escreverSuaMSG"></textarea>\
                <div class="mesControls">\
                    <div class="sendBtn sendwidget">\
                        <input type="submit" name="sendMessage" class="buttonM bLightBlue" value="Enviar" onclick="sendComment(\''+idPost+'\');" />\
                    </div>\
                    <div class="clear"></div>\
                </div>\
            </div>\
        </div>\
        <!-- End Post -->';
    if(localPush==='top'){
        console.debuglog("Colocando post no topo da timeline");
        $('#content_posts').prepend(post);
    }else{
        console.debuglog("Colocando post no final da timeline");
        $('#content_posts').append(post);
    }
    
    $('.on_off :checkbox, .on_off :radio').iButton({
        labelOn: "",
        labelOff: "",
        enableDrag: false 
    });

    $("#chTranslate_"+idPost).on('change', tranlatePost);
    $('.lim').inputlimiter({ limit: 5000, boxId: 'limitingtext', boxAttach: false });
    $('.auto').elastic();
    $('.auto').trigger('update');
    //$('.on_off :checkbox[name="chboxTranslate"]')
    setDefaultLanguage();
    //montaComments(response.comments, idPost);
    
}

function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    );
}

var carregaAjustes = function(){
    console.debuglog("Função carregaAjustes");

    $('.topAlert').click(function () {
        $('.topAlert').hide();
    });

    $("#config_open").click(function() {
        $('#progressBarSend').hide();
        $('#progressBarSend').css({width: "0%"});
        $('#sidebar').show();
        $(".secNav").show();
    });

    $("#userPictureFace").click(function() {
        $('#progressBarSend').hide();
        $('#progressBarSend').css({width: "0%"});
        $('#sidebar').show();
        $(".secNav").show();
    });

    $('.iTop').click(function () {
        //$('.altMenu').slideToggle({direction: "top"}, 100);
        if(window.localStorage.getItem('fnome')!=="undefined" && window.localStorage.getItem('fnome')!=="" ){
            $('.altMenu').hide();
            $('.topAlert').hide();
            $(".secNav").hide();
            $('#sidebar').slideToggle(100);
        }
    });

    //===== Easy tabs =====//
    
    $('#tab-container').easytabs({
        animationSpeed: 300,
        collapsible: false,
        tabActiveClass: "clicked"
    });

    $('.on_off :checkbox, .on_off :radio').iButton({
        labelOn: "",
        labelOff: "",
        enableDrag: false 
    });

    //===== Top panel search field =====//
    
    $('.userNav a.search').click(function () {
        $('.topSearch').fadeToggle(150);
    });

    //===== Animated dropdown for the right links group on breadcrumbs line =====//
    
    $('.breadLinks ul li').click(function () {
        $(this).children("ul").slideToggle(150);
    });
    
    $(document).bind('click', function(e) {
        var $clicked = $(e.target);
        if (! $clicked.parents().hasClass("has"))
        $('.breadLinks ul li').children("ul").slideUp(150);
    });

    // Dropdown
    $('.dropdown-toggle').dropdown();

    //===== Autogrowing textarea =====//
    
    $('.auto').elastic();
    $('.auto').trigger('update');

    //===== Add classes for sub sidebar detection =====//
    
    if ($('div').hasClass('secNav')) {
        $('#sidebar').addClass('with');
        //$('#content').addClass('withSide');
    }else {
        $('#sidebar').addClass('without');
        $('#content').css('margin-left','100px');//.addClass('withoutSide');
        $('#footer > .wrapper').addClass('fullOne');
    };

    $('.iButton').click(function () {
        if(window.localStorage.getItem('fnome')!=="undefined" && window.localStorage.getItem('fnome')!=="" ){
            $('#sidebar').hide();
        }
        $('.altMenu').slideToggle(100);
    });

    $('.iRefresh').click(function () {
        window.location.reload();
    });

    $(".refresh").click(function(){
        window.location.reload();
    });

    $('#notificacoes').click(function() {
        $(this).fadeOut(300);
    });
    
    //===== Add class on #content resize. Needed for responsive grid =====//
    
    $('#content').resize(function () {
        var width = $(this).width();
        if (width < 769) {
            $(this).addClass('under');
        }else { 
            $(this).removeClass('under') 
        }
    }).resize(); // Run resize on window load

    //===== Tabs =====//
        
    $.fn.contentTabs = function(){ 
        $(this).find(".tab_content").hide(); //Hide all content
        $(this).find("ul.tabs li:first").addClass("activeTab").show(); //Activate first tab
        $(this).find(".tab_content:first").show(); //Show first tab content
        $("ul.tabs li").click(function() {
            $(this).parent().parent().find("ul.tabs li").removeClass("activeTab"); //Remove any "active" class
            $(this).addClass("activeTab"); //Add "active" class to selected tab
            $(this).parent().parent().find(".tab_content").hide(); //Hide all tab content
            var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
            $(activeTab).show(); //Fade in the active content
            return false;
        });
    };

    $("div[class^='widget']").contentTabs(); //Run function on any div with class name of "Content Tabs"
    
    //== Adding class to :last-child elements ==//
        
    $(".subNav li:last-child a, .formRow:last-child, .userList li:last-child, table tbody tr:last-child td, .breadLinks ul li ul li:last-child, .fulldd li ul li:last-child, .niceList li:last-child").addClass("noBorderB")

    //===== User nav dropdown =====//       
    
    $('a.leftUserDrop').click(function () {
        $('.leftUser').slideToggle(200);
    });
    
    $(document).bind('click', function(e) {
        var $clicked = $(e.target);
        if (! $clicked.parents().hasClass("leftUserDrop"))
        $(".leftUser").slideUp(200);
    });
    
    //===== Tooltips =====//

    $('.tipN').tipsy({gravity: 'n',fade: true, html:true});
    $('.tipS').tipsy({gravity: 's',fade: true, html:true});
    $('.tipW').tipsy({gravity: 'w',fade: true, html:true});
    $('.tipE').tipsy({gravity: 'e',fade: true, html:true});

    $("button").click(function(e){
        var ns = $(this).attr('id').match(/(s\d)\-(\w+)$/);
        if (ns != null)
            $('#'+ns[1]).spinner( (ns[2] == 'create') ? opts[ns[1]] : ns[2]);
    });
    
    //===== Form elements styling =====//
    
    $("select, .check, .check :checkbox, input:radio, input:file").uniform();

    $('#breadcrumbs').xBreadcrumbs();

    //===== Chosen plugin =====//
        
    $(".select").chosen(); 

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });
    // Limita o tamanho dos textarea a 5000 caracteres
    $('.lim').inputlimiter({ limit: 5000, boxId: 'limitingtext', boxAttach: false });
    $('.limName').inputlimiter({ limit: 60, boxId: 'limitingtext', boxAttach: false });
    //===== Autotabs. Inline data rows =====//
    $('.onlyAlphaNumeric').autotab_magic().autotab_filter({ format: 'alphanumeric', uppercase: false });
    
    $('#dialogPost').dialog({
        autoOpen: false, 
        width: 400,
        title: '<span data-i18n="criarPostagem.titulo"></span>',
        modal: true,
        buttons: {
            "Cancel": function() {
                removeAllFilesPost();
                closePostDialog();
            },
            "Send": function() {
                validePostsToSend();
            }
        }
    });

    if(!isDeviceMobile()){
        console.debuglog("Detectou que não se trata de dispositivo movel, entao seta a dialog como não modal");
        $('#dialogPost').dialog({
            autoOpen: false, 
            width: 400,
            title: '<span data-i18n="criarPostagem.titulo"></span>',
            modal: false,
            buttons: {
                "Cancel": function() {
                    removeAllFilesPost();
                    closePostDialog();
                },
                "Send": function() {
                    validePostsToSend();
                }
            }
        });
    }
        
    $('#dialogContact').dialog({
        autoOpen: false, 
        width: 400,
        title: '<span data-i18n="criarContato.titulo"></span>',
        modal: true,
        buttons: {
            "Cancel": function() {
                closeContactDialog();
            },
            "Save": function() {
                valideContactToSave();
            }
        }
    });
       
     
    $('#post_open').click(function () {
        console.debuglog("Função do evento click do botao post_open");
        $("#selectBoxShare").attr("data-post", 'newpost');
        $("#selectBoxShare").appendTo( "#showContactsDivNewPost" );
        $("#btnSharePost").hide();
        $('#inputFilePost').val("");
        $('#inputFilePost').data('uri', '');
        $("#selectBoxShare").show();
        $('#btPostArquivo').click();
        $("#preloadPost").empty();
        $('#progressBarPost').hide();
        $('#progressBarPost').css({width: "0%"});
        mostraBotoesPost();
        $('#dialogPost').dialog('open');
        ajustInputImgPost()
        hideLoader('loaderPost');
        return false;
    });

    $('#contact_open').click(function () {
        console.debuglog("Função do evento click do botao contact_open");
        $("#newContactName").val("");
        $("#newContactEmail").val("");
        mostraBotoesContact();
        $('#dialogContact').dialog('open');
        hideLoader('loaderContact');
        return false;
    });

    $('#addImgPost').click(function(){
        var link = $(findUrls($("#newPostURL_IMG").val())).get(0) || "";
        if(link!==""){
            if($('#chboxUploadLink').is(':checked')){
                console.debuglog("Detectou que o link se trata de um video");           
                $('<img class="preload" data-url="'+link+'" />').attr("src", "images/icons/icon-video.png").load(onLoadImg);
            }else{
                console.debuglog("Detectou que o link não se trata de um video");           
                $('<img class="preload" data-url="'+link+'" />').attr("src", link).load(onLoadImg).error(onErrorImg);
            }
            console.debuglog("Adicionando link a lista: "+link);
        }else{
            alert(i18n.t("alertas.linkInvalido"));
        }
    });

    $('#btPostLink').click(function() {
        console.debuglog("Função click de btPostLink");
        $('#toPostIMGUpload').hide();
        $('#toPostIMGLinkPreload').show();
        $('#btSelectSrcImage').data('i18n', 'botao.link');
        $('#btSelectSrcImage').text(i18n.t('botao.link'));
        $('#btSelectSrcImageIcon').removeClass();
        $('#btSelectSrcImageIcon').addClass('icos-link');
        ajustInputImgPost();
    });

    $('#btPostArquivo').click(function() {
        console.debuglog("Função click de btPostArquivo");
        $('#toPostIMGUpload').show();
        $('#toPostIMGLinkPreload').hide();
        $('#btSelectSrcImage').data('i18n', 'botao.arquivo');
        $('#btSelectSrcImage').text(i18n.t('botao.arquivo'));
        $('#btSelectSrcImageIcon').removeClass();
        $('#btSelectSrcImageIcon').addClass('icos-files');
    });

    $('#btPostCamera').click(function() {
        console.debuglog("Função click de btPostCamera");
        if(isDeviceMobile()){
            $('#toPostIMGUpload').show();
            $('#toPostIMGLinkPreload').hide();
            $('#btSelectSrcImage').data('i18n', 'botao.camera');
            $('#btSelectSrcImage').text(i18n.t('botao.camera'));
            $('#btSelectSrcImageIcon').removeClass();
            $('#btSelectSrcImageIcon').addClass('icos-camera');
            console.debuglog("Chamando recurso de câmera");
            navigator.camera.getPicture(function (imageURI) {
                console.debuglog("Recurso camera retornou uma URI: ");
                console.debuglog(imageURI);
                uploadPhoto(imageURI, 'inputFilePost', 'imgToPost');
            }, onFailCamera, { 
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA
            });
        }else{
            console.debuglog("Detectou que não se trata de um dipositivo mobile, não podendo chamar recurso de câmera");
            alert(i18n.t("alertas.falhaPegarImagem"));
        }
    });

    $('#btSendArquivo').click(function() {
        console.debuglog("Função click de btSendArquivo");
        $('#btSelectSrcImageSend').data('i18n', 'botao.doDispositivo');
        $('#btSelectSrcImageSend').text(i18n.t('botao.doDispositivo'));
        $('#btSelectSrcImageIconSend').removeClass();
        $('#btSelectSrcImageIconSend').addClass('icos-files');
    });

    $('#btSendCamera').click(function() {
        console.debuglog("Função click de btSendCamera");
        if(isDeviceMobile()){
            $('#btSelectSrcImageSend').data('i18n', 'botao.daCamera');
            $('#btSelectSrcImageSend').text(i18n.t('botao.daCamera'));
            $('#btSelectSrcImageIconSend').removeClass();
            $('#btSelectSrcImageIconSend').addClass('icos-camera');
            console.debuglog("Chamando recurso de câmera");
            navigator.camera.getPicture(function (imageURI) {
                console.debuglog("Recurso camera retornou uma URI: ");
                console.debuglog(imageURI);
                uploadPhoto(imageURI, 'inputFileSend', 'imgToSend');
            }, onFailCamera, { 
                quality: 25,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA
            });
        }else{
            console.debuglog("Detectou que não se trata de um dipositivo mobile, não podendo chamar recurso de câmera");
            alert(i18n.t("alertas.falhaPegarImagem"));
        }
    });
}

/*var enabeBackgroundMode = function(){
    console.debuglog("Função enabeBackgroundMode")
    console.debuglog("DevReady: "+passDevReady+", i18n: "+passI18n)
    if(passDevReady && passI18n){
        console.debuglog("Ativou backgroundMode");
        // Android customization
        cordova.plugins.backgroundMode.setDefaults({title: titleBackgroud, text: msgBackground});
        // Enable background mode
        cordova.plugins.backgroundMode.enable();
    }
}*/

$(function() {

    console.debuglog("**************************************** Modo Debug ativado ****************************************");
    console.debuglog("BUILD NUMBER: "+BUILD_NUMBER);
    
    carregaAjustes();

    console.debuglog("Iniciando plugin de internacionalização i18n");
    i18n.init({
            lng: "en-US",
            fallbackLng: false, //fallback quando não definir linguagem
            debug: DEBUG, //debug do plugin
            fixLng: true, //preserva o cookie quando a linguagem for definida
            load: 'current' //define a forma correta de declarar linguagens
        }, function(translation) {
            console.debuglog("Internacionalização OK");
            passI18n = true;
            $('[data-i18n]').i18n();
            //enabeBackgroundMode();
        }
    );

    setDefaultLanguage();

    if (isDeviceMobile()) {
        titleBackgroud = i18n.t("alertas.tituloBackground");
        msgBackground = i18n.t("alertas.textoBackground");
        console.debuglog("Detectou que se trata de um dispositivo móvel");
        loadScript('cordova.js');
        loadScript('telephonenumber.js');
        $('#inputFilePost').on('click', function(evt){evt.preventDefault()});
        $('#inputFileSend').on('click', function(evt){evt.preventDefault()});
    }else {
        //Setando modo DEBUG - variavel em js/debug.js
        DEBUG = false;
        console.debuglog("Detectou que se trata de um navegador em um computador");
        window.localStorage.setItem('email', 'jeancaraca@gmail.com');
        //window.localStorage.setItem('email', 'jeanmarcosdarosa@gmail.com');
        //window.localStorage.setItem('email', 'contato@jeanmarcos.com.br');
        //addressServer="http://localhost:8080";
        if(getURLParameter("server")==="local"){
            addressServer="http://localhost:8080";
        }
        var parUser = getURLParameter("user");
        if(parUser!=="" && parUser!=="null"){
            window.localStorage.setItem('email', parUser);
        }
        window.localStorage.setItem('serialdevice', '362edd887bfd3839');
        window.localStorage.setItem('device', 'Navegador em um PC');
        $('#showIdUser').text(window.localStorage.getItem('email'));
        onLoadPG();
    }

    console.debuglog("SERVIDOR: "+addressServer);

});