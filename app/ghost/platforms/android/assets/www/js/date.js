function elapsedTime (dateBegin){
	var diff = diffTime || 0;
	if(dateBegin){
		dateBegin = dateBegin.replace("T", " ");
		dateBegin = dateBegin.substr(0, 19);
		var d1 = new Date(new Date().getTime()-diff);
		//var d2 = new Date( d1.getUTCFullYear(), d1.getUTCMonth(), d1.getUTCDate(), d1.getUTCHours(), d1.getUTCMinutes(), d1.getUTCSeconds() );
	    var ageInSeconds = (d1.getTime() - new Date(dateBegin).getTime()) / 1000;
	    if (ageInSeconds < 0) {
	        return i18n.t("posts.timeAgora");
	    }
	    if (ageInSeconds < 60) {
	        var n = ageInSeconds;
	        return i18n.t("posts.timeAlgunsSegundos");
	    }
	    if (ageInSeconds < 60 * 60) {
	        var n = Math.floor(ageInSeconds/60);
	        return n + i18n.t("posts.timeMinuto") + ((n>1)?'s':'') + i18n.t("posts.timeAtras");
	    }
	    if (ageInSeconds < 60 * 60 * 24) {
	        var n = Math.floor(ageInSeconds/60/60);
	        return n + i18n.t("posts.timeHora") + ((n>1)?'s':'') + i18n.t("posts.timeAtras");
	    }
	    if (ageInSeconds < 60 * 60 * 24 * 7) {
	        var n = Math.floor(ageInSeconds/60/60/24);
	        return n + i18n.t("posts.timeDia") + ((n>1)?'s':'') + i18n.t("posts.timeAtras");
	    }
	    if (ageInSeconds < 60 * 60 * 24 * 31) {
	        var n = Math.floor(ageInSeconds/60/60/24/7);
	        return n + i18n.t("posts.timeSemana") + ((n>1)?'s':'') + i18n.t("posts.timeAtras");
	    }
	    if (ageInSeconds < 60 * 60 * 24 * 365) {
	        var n = Math.floor(ageInSeconds/60/60/24/31);
	        return n + ((n>1)?i18n.t("posts.timeMeses"):i18n.t("posts.timeMes")) + i18n.t("posts.timeAtras");
	    }
	    var n = Math.floor(ageInSeconds/60/60/24/365);
	    return n + i18n.t("posts.timeAno") + ((n>1)?'s':'') + i18n.t("posts.timeAtras");
	}else{
		return '';
	}
}