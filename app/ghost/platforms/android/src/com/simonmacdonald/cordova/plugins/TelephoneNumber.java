package com.simonmacdonald.cordova.plugins;

import java.text.Normalizer;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Base64;

public class TelephoneNumber extends CordovaPlugin {
	
	public static String removerAcentos(String str) {
	    return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        if (action.equals("getSerial")) {
            TelephonyManager telephonyManager = (TelephonyManager)this.cordova.getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            String result = (telephonyManager.getSimSerialNumber()+telephonyManager.getNetworkOperator()+telephonyManager.getNetworkCountryIso()).toUpperCase();
            if (result != null) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
               return true;
            }
        }else if (action.equals("getListContacts")) {
            String result = "";
            ContentResolver cr = this.cordova.getActivity().getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
            if (cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    Cursor cur1 = cr.query( ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null); 
                    //Cursor cur2 = cr.query( ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    //String number = "";
                    //while (cur2.moveToNext()) {
                    //	number=cur2.getString(cur2.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    //	System.out.println(number);
                    //}
                    while (cur1.moveToNext()) { 
                        String name=cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        if(email!=null && name!=null){
                        	//if(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)).equals("1")){
                        		try {
                            		String name64   = Base64.encodeToString(removerAcentos(name).getBytes("latin-1"), Base64.DEFAULT).replace("=", "");
                            		String email64  = Base64.encodeToString(email.getBytes("latin-1"), Base64.DEFAULT).replace("=", "");
                            		result+= name64+":"+email64+"|";
								} catch (Exception e) {
									// TODO: handle exception
								}
                        	//}                          
                        }
                    } 
                    cur1.close();
                }
            }
            if (result != null) {
            	if (result.length() > 0 && result.charAt(result.length()-1)=='|') {
            		result = result.substring(0, result.length()-1);
        	    }
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
               return true;
            }
        }else if(action.equals("getEmail")){
            AccountManager manager = (AccountManager)this.cordova.getActivity().getSystemService(Context.ACCOUNT_SERVICE);
            Account[] list = manager.getAccounts();
            String strGmail = "";
            if(list.length>0){
                strGmail = list[0].name;
            }
            for(Account account:list){
                String possibleEmail = account.name;
                String type = account.type;
                if (type.equals("com.google")) {
                    strGmail = possibleEmail;
                    break;
                }
            }
            if (strGmail != null) {
            		callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, strGmail));
               return true;
            }
        }
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
        return false;
    }
}
