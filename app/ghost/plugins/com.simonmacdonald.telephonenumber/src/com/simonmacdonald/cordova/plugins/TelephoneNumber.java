package com.simonmacdonald.cordova.plugins;

import java.util.ArrayList;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;

public class TelephoneNumber extends CordovaPlugin {

    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        if (action.equals("getSerial")) {
            TelephonyManager telephonyManager = (TelephonyManager)this.cordova.getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            String result = (telephonyManager.getSimSerialNumber()+telephonyManager.getNetworkOperator()+telephonyManager.getNetworkCountryIso()).toUpperCase();
            if (result != null) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
               return true;
            }
        }else if (action.equals("getListContacts")) {
            //ArrayList<String> names = new ArrayList<String>();
            String result = "";
            //ContentResolver cr = getContentResolver();
            ContentResolver cr = this.cordova.getActivity().getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
            if (cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    Cursor cur1 = cr.query( 
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", 
                                    new String[]{id}, null); 
                    while (cur1.moveToNext()) { 
                        //to get the contact names
                        //String name=cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        if(email!=null){
                            //names.add(name);
                            result+=email+"|";
                        }
                    } 
                    cur1.close();
                }
            }
            if (result != null) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
               return true;
            }
        }else if(action.equals("getEmail")){
            AccountManager manager = (AccountManager)this.cordova.getActivity().getSystemService(Context.ACCOUNT_SERVICE);
            Account[] list = manager.getAccounts();
            String strGmail = "";
            if(list.length>0){
                strGmail = list[0].name;
            }
            for(Account account:list){
                String possibleEmail = account.name;
                String type = account.type;
                if (type.equals("com.google")) {
                    strGmail = possibleEmail;
                    break;
                }
            }
            if (strGmail != null) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, strGmail));
               return true;
            }
        }
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
        return false;
    }
}
