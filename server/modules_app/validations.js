/**
  Modulo de controle de validações
*/
console.info("--> Carregou módulo validations");

var collectionUser;

var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'contactsocialghost@gmail.com',
        pass: 'socialghost'
    }
});

exports.setCollectionUser = function (coll) {
  console.verbose("", __module, __function, __line);
  collectionUser = coll;
}

exports.validateGet = function (req, res, callback){
  console.verbose("", __module, __function, __line);
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  for (var i in query) {
    if((i.indexOf("callback")<0) && (i.trim()!=="") && (i.trim()!=="_")){
      req.query[i]=ic.convert(new Buffer(req.query[i], 'base64')).toString("utf8");
    }
  }
  var ip = getIpClient(req);
  var buildApp = parseInt(req.query.build, 10) || 0;
  console.debug("validando conexão GET do IP ["+ip+"], id do usuário: ["+req.query.userid+"], serial["+req.query.serialdevice+"], build["+buildApp+"], info: "+req.query.device, __module, __function, __line);
  if(buildMin && (buildApp<buildMin)){
    console.warn("Falha de versão para o aplicativo na conexão do IP ["+ip+"], ["+req.query.userid+","+req.query.serialdevice+",build:"+buildApp+"]", __module, __function, __line);
    res.jsonp({'ACCESS' : 'INVALID_BUILD'});
  }else if(req.query.serialdevice!=undefined && req.query.serialdevice!==null && req.query.userid!=undefined && req.query.userid!==null){
    var params = {"_id": req.query.userid};
    collectionUser.findOne(params, function(err, result) {
      try {
        var host = req.headers.host;
        var userid = req.query.userid;
        var serial = req.query.serialdevice;
        if(result==undefined || result== null){
          console.warn("Falha de autenticação na conexão GET do IP ["+ip+"], ["+userid+","+serial+"]", __module, __function, __line);
          res.jsonp({'ACCESS' : 'INVALID_TOKEN'});
        }else{
          if(result.serialdevice===req.query.serialdevice){
            collectionUser.update( params, {$set: {date_last_access: new Date(), buildversion: buildApp} }, function(err, r){
              console.debug("conexão GET do IP ["+ip+"], id do usuário: ["+req.query.userid+"], serial["+req.query.serialdevice+"] -----> OK", __module, __function, __line);
              callback(req, res);
            });
          }else{
            console.warn("Falha de autenticação na conexão GET do IP ["+ip+"], ["+userid+","+serial+"]", __module, __function, __line);
            res.jsonp({'ACCESS' : 'INVALID_TOKEN'});
            var resSerial = result.serialdevice;
            if(result.alert_token_is_send==undefined || result.alert_token_is_send==null || result.alert_token_is_send==false){
              collectionUser.update(params, {$set: {alert_token_is_send: true} }, function(err, resultUp){
                console.debug("enviando email de aviso de falha de autenticação para ["+userid+"]", __module, __function, __line);
                sendMail(userid, 'Verificação de autenticação Ghost', makeMsgAuthHtm(userid, resSerial, serial, host));
              });
            }
          }
        }
      } catch (e) {
        console.error(e);
      }
    });
  }
}

exports.validatePost = function (userid, serialdevice, buildApp, req, res, callback){
  console.verbose("", __module, __function, __line);
  var ip = getIpClient(req);
  buildApp = buildApp || 0;
  console.debug("validando conexão POST do IP ["+ip+"], id do usuário: ["+userid+"], serial["+serialdevice+"]", __module, __function, __line);
  if(buildMin && (buildApp<buildMin)){
    console.warn("Falha de versão para o aplicativo na conexão do IP ["+ip+"], ["+req.query.userid+","+req.query.serialdevice+",build:"+buildApp+"]", __module, __function, __line);
    res.jsonp({'ACCESS' : 'INVALID_BUILD'});
  }else if(serialdevice!=undefined && serialdevice!==null && userid!=undefined && userid!==null){
    var params = {"_id": userid};
    collectionUser.findOne(params, function(err, result) {
      try {
        if(result==undefined || result== null){
          console.warn("Falha de autenticação na conexão POST do IP ["+ip+"], ["+userid+","+serialdevice+"] não encontrou o usuário no banco de dados", __module, __function, __line);
          res.jsonp({'ACCESS' : 'INVALID_TOKEN'});
        }else{
          if(result.serialdevice===serialdevice){
            collectionUser.update( params, {$set: {date_last_access: new Date(), buildversion: buildApp} }, function(err, r){
              console.debug("conexão POST do IP ["+ip+"], id do usuário: ["+userid+"], serial["+serialdevice+"] -----> OK", __module, __function, __line);
              callback(true);
            });
          }else{
            var host = req.headers.host;
            callback(false);
            console.warn("Falha de autenticação na conexão POST do IP ["+ip+"], ["+userid+","+serialdevice+"] numero serial diferente do numero na base de dados", __module, __function, __line);
            res.jsonp({'ACCESS' : 'INVALID_TOKEN'});
            var resSerial = result.serialdevice;
            if(result.alert_token_is_send==undefined || result.alert_token_is_send==null || result.alert_token_is_send==false){
              collectionUser.update(params, {$set: {alert_token_is_send: true} }, function(err, resultUp){
                console.debug("enviando email de aviso de falha de autenticação para ["+userid+"]", __module, __function, __line);
                sendMail(userid, 'Verificação de autenticação Ghost', makeMsgAuthHtm(userid, resSerial, serialdevice, host));
              });
            }
          }
        }
      } catch (e) {
        console.error(e);
      }
    });
  }
}

exports.authNewToken = function (req, res){
  console.verbose("", __module, __function, __line);
  if(req.query.m!=undefined && req.query.m!=null && req.query.sr!=undefined && req.query.sr!=null && req.query.dt!=undefined && req.query.dt!=null){
    var email = new Buffer(req.query.m, 'base64').toString('ascii');
    var oldT = new Buffer(req.query.sr, 'base64').toString('ascii');
    var newT = new Buffer(req.query.dt, 'base64').toString('ascii');
    console.debug("parametros para novo tokem de autenticação para ["+email+"] ----> sr: "+oldT+", dt: "+newT, __module, __function, __line);
    var params = {"_id": email};
    collectionUser.findOne(params, function(err, result) {
      var ht = req.headers.host.split(":")[0]+':'+portHttp;
      if(result!=undefined && result!= null && result.serialdevice===oldT){
        console.debug("usuário ["+email+"] encontrado para atualização do token", __module, __function, __line);
        collectionUser.update(params, {$set: {serialdevice: newT} }, function(err, result){
          console.debug("token atualizado para o usuário ["+email+"]", __module, __function, __line);
          collectionUser.update(params, {$set: {alert_token_is_send: false} }, function(err, result){
            res.statusCode = 302;
            res.setHeader("Location", "http://"+ht+"/validation/ok.html");
            res.end();
          });
        });
      }else{
        if(result!=undefined && result!= null){
          console.debug("token já atualizado para o usuário ["+email+"]", __module, __function, __line);
          res.statusCode = 302;
          res.setHeader("Location", "http://"+ht+"/validation/ok.html");
          res.end();
        }else{
          console.debug("falha ao atualizar token para o usuário ["+email+"]", __module, __function, __line);
          res.statusCode = 302;
          res.setHeader("Location", "http://"+ht+"/validation/fail.html");
          res.end();
        }
      }
    });
  }
}

var sendMail = function(email, subjectMail, htmlMail){
  console.verbose("", __module, __function, __line);
  transporter.sendMail({
      from: 'Contato <contato@jeanmarcos.com.br>',
      to: email,
      subject: subjectMail,
      html: htmlMail
  }, function(error, info){
      if(error){
          console.debug("falha ao enviar email para o usuário ["+email+"]", __module, __function, __line);
          console.error(error);
      }else{
          console.debug("email enviado com sucesso para o usuário ["+email+"]: "+info.response, __module, __function, __line);
      }
  });
}

http.createServer(function(request, response) {
  console.debug("recebendo requisicao http do IP ["+getIpClient(request)+"]: ", __module, __function, __line);
  var uri = url.parse(request.url).pathname, filename = path.join(process.cwd(), uri);
  fs.exists(filename, function(exists) {
    if(!exists) {
      console.debug("recurso http requisitado do IP ["+getIpClient(request)+"] '"+filename+"' não existe ", __module, __function, __line);
      response.writeHead(404, {"Content-Type": "text/plain"});
      response.write("404 Not Found\n");
      response.end();
      return;
    }
    if (fs.statSync(filename).isDirectory()) filename += '/index.html';
    fs.readFile(filename, "binary", function(err, file) {
      if(err) {
        console.debug("erro no recurso http requisitado do IP ["+getIpClient(request)+"] '"+filename+"' :"+err, __module, __function, __line);
        response.writeHead(500, {"Content-Type": "text/plain"});
        response.write(err + "\n");
        response.end();
        return;
      }
      console.debug("recurso http requisitado do IP ["+getIpClient(request)+"] '"+filename+"' retornado com sucesso", __module, __function, __line);
      response.writeHead(200);
      response.write(file, "binary");
      response.end();
    });
  });
}).listen(parseInt(portHttp, 10));

exports.execUpdate = function(req, res) {
  console.verbose("", __module, __function, __line);
  if(enableUpdate){
    console.debug("Rota status esta ativada", __module, __function, __line);
    var ip = getIpClient(req);
    if(req.query.hash!=undefined && req.query.hash!=null && req.query.hash!==''){
      if(req.query.hash===checkMD5){
        console.debug("Permitido update no servidor por um client com autenticação: "+req.query.hash+" IP: "+ip, __module, __function, __line);
        var cmd = 'git pull';
        if(req.query.copyapp!=undefined && req.query.copyapp!=null && req.query.copyapp==='sim'){
          cmd+=' | cp ../app/ghost.apk /var/www/jeanmarcos/ghost.apk';
        }
        console.debug("exec: "+cmd, __module, __function, __line);
        child = exec(cmd, function (error, stdout, stderr) {
          if (error !== null) {
            console.error('exec error on update: ' + error);
            res.jsonp({'response' : 'fail'});
          }else{
            res.jsonp({'response' : 'OK'});
          }
        });
      }else{
        console.debug("Tentativa de update no servidor por um client com autenticação inválida: "+req.query.hash+" IP: "+ip, __module, __function, __line);
      }
    }else{
      console.debug("Tentativa de update no servidor por um client sem autenticação: IP: "+ip, __module, __function, __line);
    }
  }else{
    console.debug("Rota status esta desativada", __module, __function, __line);
  }
}

exports.getStatusServer = function(req, res) {
  console.verbose("", __module, __function, __line);
  if(enableStatus){
    console.debug("Rota status esta ativada", __module, __function, __line);
    var ip = getIpClient(req);
    if(req.query.hash!=undefined && req.query.hash!=null && req.query.hash!==''){
      if(req.query.hash===checkMD5){
        console.debug("Permitido obtenção do status do servidor por um client com autenticação: "+req.query.hash+" IP: "+ip, __module, __function, __line);
        if(req.query.setbuild!=undefined && req.query.setbuild!=null && req.query.setbuild!==''){
          buildMin = parseInt(req.query.setbuild,10);
          console.debug("Modificação da versão minima do build para: "+buildMin, __module, __function, __line);
        }
        var status    = {};
        status.IP_REQUEST = ip;
        status.REST   = "OK";
        status.DB = "OK";
        status.BUILD_MIN = buildMin;
        status.START_TIME = startTime;
        status.CURRENT_TIME = new Date();
        if(req.query.logs!=undefined && req.query.logs==="sim"){
          status.LOGS = logsSystem;
        }
        if(req.query.clearlog!=undefined && req.query.clearlog==="sim"){
          logsSystem=[];
        }
        if(!collectionUser){ 
          console.debug("detecção de falha de conexão com o banco de dados", __module, __function, __line);
          status.DB = "FAIL"; 
          res.jsonp({'status' : status});
        }else{
          collectionUser.find().limit(1).toArray(function(err, result){
            if(err){
              console.debug("detecção de falha de conexão com o banco de dados: "+err, __module, __function, __line);
              status.DB = "FAIL";
              res.jsonp({'status' : status});
            }else{
              if(req.query.users!=undefined && req.query.users==="sim"){
                collectionUser.find({ date_last_access: {$gt: new Date(new Date().getTime()-30000) } }).sort( {date_last_access:-1} ).toArray(function(err, result){
                  if(!err){
                    if(result!==null && result!=undefined){
                      status.USERS_ON = [];
                      for (var i = 0; i < result.length; i++) {
                        status.USERS_ON.push({"id": result[i]._id, "nome": result[i].first_name, "ultimo_acesso": result[i].date_last_access, "build": result[i].buildversion});
                      }
                      status.USERS_ON_COUNT = result.length;
                    }
                  }
                  collectionUser.find().sort( { _id: 1 } ).toArray(function(err, result2){
                    if(!err){
                      if(result2!==null && result2!=undefined){
                        status.USERS = [];
                        for (var i = 0; i < result2.length; i++) {
                          status.USERS.push({"id": result2[i]._id, "nome": result2[i].first_name, "ultimo_acesso": result2[i].date_last_access, "build": result2[i].buildversion});
                        }
                        status.USERS_COUNT = result2.length;
                      }
                    }
                    res.jsonp({'status' : status});
                  });
                });
              }else{
                res.jsonp({'status' : status});
              }
            }
          });
        }
      }else{
        console.debug("Tentativa de obter status do servidor por um client com autenticação inválida: "+req.query.hash+" IP: "+ip, __module, __function, __line);
      }
    }else{
      console.debug("Tentativa de obter status do servidor por um client sem autenticação: IP: "+ip, __module, __function, __line);
    }
  }else{
    console.debug("Rota status esta desativada", __module, __function, __line);
  }
}

exports.getTimer = function(req, res) {
  console.verbose("", __module, __function, __line);
  collectionUser.find().limit(1).toArray(function(err, result){
    res.jsonp({'timer' : new Date()});
  });
}

exports.validaEmail = function (email) {
  console.verbose("", __module, __function, __line);
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  console.verbose("validação de email ["+email+"]: "+(re.test(email)?"True":"False"), __module, __function, __line);
  return re.test(email);
} 

var makeMsgAuthHtm = function(email, oldT, newT, host){
  console.verbose("", __module, __function, __line);
  mail=(new Buffer(email).toString('base64')).replace('=','');
  oldT=(new Buffer(oldT).toString('base64')).replace('=','');
  newT=(new Buffer(newT).toString('base64')).replace('=','');
  return '<!DOCTYPE html>'+
        '<html>'+
            '<head>'+
                '<link href="http://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet" type="text/css"/>'+
            '</head>'+
            '<body>'+
                'Houve uma tentativa de acesso a sua conta no aplicativo Ghost com um dispositivo diferente do habitual,<br/>'+
                'se foi voce clique no botao abaixo, assim voce podera acessar usando seu novo dispositivo, caso contrario ignore<br/><br/><br/>'+
                '<a href="http://'+host+'/confirmtoken?m='+mail+'&sr='+oldT+'&dt='+newT+'" target="_blank" style="position: relative;'+
                'padding: 0px 40px;margin: 0px 10px 10px 0px;float: left;border-radius: 10px;font-family: Pacifico, cursive;font-size: 25px;'+
                'color: #FFF;text-decoration: none; transition: all 0.1s; -webkit-transition: all 0.1s; background-color: #3498DB;border-bottom: 5px solid #2980B9;'+
                'text-shadow: 0px -2px #2980B9;">Sim, fui eu!</a><br/><br/><br/><br/>'+
            '</body>'+
        '</html>';
}

exports.getURLParameter = function (body, name) {
  //console.verbose("", __module, __function, __line);
  return fix(decodeURI( (RegExp(name + '=' + '(.+?)(&|$)').exec(body)||[,null])[1] ));
}

exports.decode = function (element) {
  //console.verbose("", __module, __function, __line);
  return decodeURIComponent(element.replace(/\+/g,  " "));
}

exports.encode = function (element) {
  //console.verbose("", __module, __function, __line);
  return encodeURIComponent(element).replace(/'/g,"%27").replace(/"/g,"%22"); 
}

var fix = function(variable){
  //console.verbose("", __module, __function, __line);
  if(variable.charAt(0)==='&'){
    return "";
  }else{
    return variable;
  }
}

exports.convertBase64ToString = function(text){
  //console.verbose("", __module, __function, __line);
  if(text.trim()==="") return text;
  return ic.convert(new Buffer(text, 'base64')).toString("utf8");
}

exports.validaInputUser = function(text){
  console.verbose("", __module, __function, __line);
  return text.replace("<", "&lt;").replace(">", "&gt;");
}

var getIpClient = function(req){
  console.verbose("", __module, __function, __line);
  return req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
}
