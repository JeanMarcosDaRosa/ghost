/**
	Modulo de controle de postagens
*/
console.info("--> Carregou módulo to_posting");

var collection, collectionToPosting, collectionUser;

exports.setCollection = function (coll) {
	console.verbose("", __module, __function, __line);
	collection = coll;
};
exports.setCollectionToPosting = function (coll) {
	console.verbose("", __module, __function, __line);
	collectionToPosting = coll;
};
exports.setCollectionUser = function (coll) {
	console.verbose("", __module, __function, __line);
	collectionUser = coll;
};

// Rota GET para listar os posts
exports.listLast = function(req, res){
	console.verbose("", __module, __function, __line);
	val.validateGet(req, res, function(req, res){
		// Fazendo uma consulta no banco de dados
		var iduser = req.query.userid;
		var onlyold = req.query.onlyoldposts;
		var params = {"iduser": iduser};
		var lim = (req.query.limit!==null?parseInt(req.query.limit, 10):5);
		if(onlyold==="true"){
			//{"created_time":{$gt: new Date("2014-09-05T12:45:46+0000")}}
			params = {"iduser": iduser, "created_time" : { $lt: new Date(req.query.datelastsync)}};
		}
		
		collectionToPosting.find(params).sort( { created_time: -1 } ).limit(lim).toArray(function(err, result){
			if(result!==null && result!=undefined){
				collectionUser.findOne({"_id": iduser}, function(err, resultUser) {
					var arr = [], sharedByList = {};
					if(resultUser!==undefined){
						for (var j = 0; j < result.length; j++) {
							var flag = true;
							if(resultUser.list_channels_revoke!==null && resultUser.list_channels_revoke!=undefined){
								for (var i = 0; i < resultUser.list_channels_revoke.length; i++) {
									if(resultUser.list_channels_revoke[i]==result[j].idchannel){
										flag = false;
										break;
									}
								}
							}
							if(flag){
								arr.push(new ObjectID(result[j].idpost));
								sharedByList[result[j].idpost] = [result[j].iduser_src, result[j].nameuser_src];
							}
						}
						params = {"_id": { $in: arr}};
						if(onlyold==="true"){
							//{"created_time":{$gt: new Date("2014-09-05T12:45:46+0000")}}
							params = {"_id": { $in: arr}, "created_time" : { $lt: new Date(req.query.datelastsync)}};
						}
				    	collection.find(params).sort( { created_time: -1 } ).toArray(function(err, result2){
							var arrResult=[];
							for (var i = 0; i < result2.length; i++) {
								arrResult.push({'_id':result2[i]._id, 'created_time':result2[i].created_time, 'iduser_src': sharedByList[result2[i]._id.toHexString()]});
							}
							res.jsonp({'posts' : arrResult});
						});
					}else{
						res.jsonp({'posts' : []});
					}
				});
			}else{
				res.jsonp({'posts' : []});
			}
		});

	});
};


// Rota GET para listar os novos posts
exports.listNews = function(req, res){
	console.verbose("", __module, __function, __line);
	val.validateGet(req, res, function(req, res){
		// Fazendo uma consulta no banco de dados
		var iduser = req.query.userid;
		var lim = (req.query.limit!==null?parseInt(req.query.limit, 10):5);

		collectionToPosting.find({"iduser": iduser, "synchronized": 0}).sort( { created_time: 1 } ).limit(lim).toArray(function(err, result){
			if(result!==null && result!=undefined){
				//Busca usuario no banco de dados
				collectionUser.findOne({"_id": iduser}, function(err, resultUser) {
					
					var arr = [], sharedByList = {}; // array que contera a lista de canais nao revogados

					// Percorre lista de ids de canais e compare com a lista de canis revogados
					// caso o canal não estja na lista de canais revogados, entao os posts
					// desse canal serao add para o usuario
					for (var j = 0; j < result.length; j++) {
						var flag = true;
						if(resultUser.list_channels_revoke!==null && resultUser.list_channels_revoke!=undefined){
							for (var i = 0; i < resultUser.list_channels_revoke.length; i++) {

								// Se canal revogado for igual ao canal, entao este nao sera add
								// para se buscar post
								if(resultUser.list_channels_revoke[i]===result[j].idchannel){
									flag = false;
									break;
								}
							}
						}
						// se flag for true, entao sera add os post do canal
						if(flag){
							arr.push(new ObjectID(result[j].idpost));
							sharedByList[result[j].idpost] = [result[j].iduser_src, result[j].nameuser_src];
						}
					}

					//Busca os posts dos canais selecionados e retorna no res.jsonp
					collection.find({'_id': { $in: arr }}).sort( { created_time: -1 } ).toArray(function(err, result2){
						var arrResult=[];
						for (var i = 0; i < result2.length; i++) {
							arrResult.push({'_id':result2[i]._id, 'created_time':result2[i].created_time, 'iduser_src': sharedByList[result2[i]._id.toHexString()]});
						}
						res.jsonp({'posts' : arrResult});
					});
				})
			}else{
				res.jsonp({'posts' : []});
			}
		});

	});
};


// Rota GET para confirmar sincronismo
exports.postsConfirm = function(req, res){
	console.verbose("", __module, __function, __line);
	val.validateGet(req, res, function(req, res){
		// Recebendo os parâmetros de um query string
		var posts = JSON.parse(req.query.postsRec);
		if(posts!=undefined && posts!=null && posts.length>0){
			for(var i=0;i<posts.length;i++){
			    collectionToPosting.update({"iduser":req.query.userid, "idpost": posts[i]}, {$set: {"synchronized":1}}, {multi: true} , function(err, result){});
			}
		}
		res.jsonp({'response' : 'OK'});
	});
};

exports.search = function(req, res){
	console.verbose("", __module, __function, __line);
	val.validateGet(req, res, function(req, res){
		res.jsonp({'posts' : [  {'_id': "216311481960_10152369391626961"} , {'_id': "216311481960_10152370725006961"} ] });
	});
}

/*
Fabio - Jean
*/

var translatePost = function(res, post, lang){
	console.verbose("", __module, __function, __line);
	//lang="en-US";
	//convert text to base64
	var messageBase64 = new Buffer(post.message).toString('base64');
	var langBase64 = new Buffer(lang).toString('base64');
	//passar por parametro cada texto, inclusive os comentarios, em sequencia, para depois recupera-los
	var comments = post.comments || [];
	var commentsBase64 = "";
	for (var i = 0; i < comments.length; i++) {
		commentsBase64+='"'+( new Buffer(comments[i].message).toString('base64') )+'" ';
	}
	var cmd = 'python translate.py "'+langBase64+'" "'+messageBase64+'" '+commentsBase64;
	console.debug("exec: "+cmd, __module, __function, __line);
	child = exec(cmd, function (error, stdout, stderr) {
		if (error !== null) {
			console.error('exec error: ' + error);
			console.error('SMARTFEEDTRANSLATEFAIL0');
			res.jsonp({'post' : post});
		}else{
			if(stdout.trim().indexOf('SMARTFEEDTRANSLATEFAIL')>-1){
				res.jsonp({'post' : post});
			}else{
				var result = stdout.trim().split('\n');
				var withMessagePost=false;
				if(post.message.trim()!==""){
					post.message = new Buffer(result[0], 'base64').toString('utf8');
					withMessagePost = true;
				}
				if(post.comments){
					for (var i = 0; i < post.comments.length; i++) {
						post.comments[i].message = new Buffer(result[i+(withMessagePost?1:0)], 'base64').toString('utf8');
					}
				}
				res.jsonp({'post' : post});
			}
		}
	});
}

exports.getPost = function(req, res){
	console.verbose("", __module, __function, __line);
	val.validateGet(req, res, function(req, res){
		console.debug("getPost: "+req.query.info_idpost, __module, __function, __line);
		var idPost = req.query.info_idpost;
		var lang   = req.query.translate || 'none';
		var params = {_id: new ObjectID(idPost)};
		collection.findOne(params, function(err, result){
			if(result!=null && result!=undefined){
				console.debug("Encontrou o post: "+idPost, __module, __function, __line);
				channels.getChannel(function(resultado){
					if(resultado.username==='friends' && result.iduser_src!=undefined && result.iduser_src!=null && result.iduser_src!==""){
						collectionUser.findOne({"_id": result.iduser_src}, function(err, resultUser) {
							result["status"]="OK";
							if(resultUser!=undefined && resultUser!=null ){
								var usuario = {"username" : resultUser._id, "name" : resultUser.first_name, "description" : 'friends', type: "user"};
								result["from"]=usuario;
							}else{
								var usuario = {"username" : "desconhecido", "name" : "Desconhecido", "description" : 'friends', type: "user"};
								result["from"]=usuario;
							}
							if(lang==='none'){
								res.jsonp({'post' : result});
							}else{
								translatePost(res, result, lang);
							}
						});
					}else{
						resultado["type"]="channel";
						result["from"]=resultado;
						if(lang==='none'){
							res.jsonp({'post' : result});
						}else{
							translatePost(res, result, lang);
						}
					}
				}, result.idchannel);
			}else{
				console.debug("Não encontrou o post: "+idPost, __module, __function, __line);
				res.jsonp({'post' : {_id: idPost, status: "POST_NOT_EXIST"} });
			}
		});
	});
}

exports.removePost = function(req, res){
	console.verbose("", __module, __function, __line);
	var body = '';
    req.on('data', function (data) {
        body += data;
    });
    req.on('end', function () {
    	// Pegando dados post
    	var userid = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'userid'))));
      	var serial = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'serialdevice'))));
      	var buildApp = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'build'))));
    	val.validatePost(userid, serial, buildApp, req, res,function(auth){
    		if(auth){
    			var idpost = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'idpost')));
    			collection.findOne({"_id":new ObjectID(idpost), "iduser_src": userid}, function(err, result) {
    				if(result!==null && result!=undefined){
    					if(result.iduser_src==userid){
    						var recursos = result.resources;
    						console.debug("removendo postagem e to_posting do usuario: "+userid+", postagem: "+idpost, __module, __function, __line);
	    					collection.remove({"_id":new ObjectID(idpost), "iduser_src": userid}, function(err, resultDeleted) {
	    						collectionToPosting.remove({'idpost':idpost}, function(err, numberOfRemovedDocs) {
	    							if(recursos==null || recursos==undefined || recursos.length==0){
	    								console.error("não existem recursos para serem removidos para a postagem: "+idpost, __module, __function, __line);
	    							}else{
	    								console.warn("removendo ["+recursos.length+"] recursos da postagem: "+idpost, __module, __function, __line);
	    								for (var i = 0; i < recursos.length; i++) {
	    									var fileName = recursos[i].source;
	    									if(fileName.indexOf('[SERVER]/public/')===0){
	    										fileName=fileName.substring(16);
		    									console.warn("removendo ["+fileName+"]", __module, __function, __line);
		    									if (fs.existsSync(fileName)&&(!fs.lstatSync(fileName).isDirectory())) {
											        fs.unlinkSync(fileName);
											        console.warn("["+fileName+"] --> removed", __module, __function, __line);
										            if (fs.existsSync(fileName+'-small')&&(!fs.lstatSync(fileName+'-small').isDirectory())) {
										            	console.warn("["+fileName+"-small] --> removed", __module, __function, __line);
														fs.unlinkSync(fileName+'-small');
										            }
										            if (fs.existsSync(fileName+'-thumb.png')&&(!fs.lstatSync(fileName+'-thumb.png').isDirectory())) {
										            	console.warn("["+fileName+"-thumb.png] --> removed", __module, __function, __line);
										            	fs.unlinkSync(fileName+'-thumb.png');
										            }
											    }
	    									}
	    								}
	    							}
	    							res.jsonp({'response' : {_id: idpost, status: numberOfRemovedDocs} });
						      	});
	    					});
					      }else{
					      	console.debug("removendo to_posting do usuario: "+result.iduser_src+" para o usuario: "+userid, __module, __function, __line);
				    		collectionToPosting.remove({'idpost':idpost, 'iduser': userid}, function(err, numberOfRemovedDocs) {
				    			res.jsonp({'response' : {_id: idpost, status: numberOfRemovedDocs} });
					      	});
					      }
			    	}else{
			    		console.debug("Não encontrou o post: "+idpost, __module, __function, __line);
						res.jsonp({'response' : {_id: idpost, status: "POST_NOT_EXIST"} });
						collectionToPosting.remove({'idpost':idpost}, function(err, numberOfRemovedDocs) {});
			    	}
			    });
		    }
		});
    });
}

var geraNewPost = function(body, callback){
	console.verbose("", __module, __function, __line);
	var idCanal = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'idchannel')));
	var mensagem = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'message')));
	var res = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'resources')));
	var idExterno = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'idextern')));
	var userid = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'userid'))));
	if(res!=undefined && res!=null && res!==""){
		res=res.replace("<", "&lt;").replace(">", "&gt;");
		res = JSON.parse(res);
	}else{
		res = undefined;
	}
	mensagem=val.validaInputUser(mensagem);
	if(idCanal==='friends' || idCanal==='amigo'){
		channels.getChannelByName(function(resultado){
			idCanal=resultado._id.toHexString();
			collection.insert({idchannel: idCanal, iduser_src: userid, message: mensagem, resources: res, extractRes: false, procSema: false, idextern: idExterno, likes: null, created_time: new Date()}, function(err, result){
				callback(result);
			});
		}, idCanal);
	}else{
		collection.insert({idchannel: idCanal.toHexString(), iduser_src: userid, message: mensagem, resources: res, extractRes: false, procSema: false, idextern: idExterno, likes: null, created_time: new Date()}, function(err, result){
			callback(result);
		});
	}
}

exports.newPost = function(req, res){
	console.verbose("", __module, __function, __line);
	var body = '';
    req.on('data', function (data) {
        body += data;
    });

    req.on('end', function () {
    	// Pegando dados post
    	var userid = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'userid'))));
      	var serial = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'serialdevice'))));
    	var buildApp = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'build'))));
    	val.validatePost(userid, serial, buildApp, req, res,function(auth){
    		if(auth){
		    	geraNewPost(body, function(result){
					res.jsonp({'response' : result[0]._id});
				});
		    }
		});
    });
}

exports.sharePost = function(req, res){
	console.verbose("", __module, __function, __line);
	var body = '';
    req.on('data', function (data) {
        body += data;
    });
    req.on('end', function () {
    	// Pegando dados post
    	var userid = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'userid'))));
      	var serial = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'serialdevice'))));
      	var nameuser = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'nameuser'))));
		var buildApp = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'build'))));
    	val.validatePost(userid, serial, buildApp, req, res,function(auth){
    		if(auth){
    			var idpost = val.decode(val.getURLParameter(body, 'idpost'));
    			var lista = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'userlist')));
    			if(idpost==='null' || idpost===''){
    				geraNewPost(body, function(result){
    					compartilharToPosting(res, userid, result[0]._id, nameuser, lista, true)
    				});
    			}else{
    				compartilharToPosting(res, userid, val.convertBase64ToString(idpost), nameuser, lista, false);
	    		}
		    }
		});
    });
}

var compartilharToPosting = function(res, userid, idpost, nameuser, lista, tosrc){
	console.verbose("", __module, __function, __line);
	if(lista===""){
		res.jsonp({'response' : 'ERROR'});
	}else{
		var listControl=[];
		if(tosrc){
			if(listControl.indexOf(userid)===-1) listControl.push(userid);
		}
		if(lista.toUpperCase().indexOf('SELF|')===-1){
			var arr = lista.split("|");
			for (var i = 0; i < arr.length; i++) {
				if(arr[i]!=undefined && arr[i]!==""){
					if(listControl.indexOf(arr[i])===-1 && val.validaEmail(arr[i])){
						listControl.push(arr[i]);
					}
				}
			}
			if(lista.toUpperCase().indexOf('TODOS1|')>-1){
				users.getListaAmigosGlobal(userid, function(result){
					if(result.comapp!=undefined){
						for (var i =  0; i < result.comapp.length; i++) {
							if(listControl.indexOf(result.comapp[i][1])===-1){
								listControl.push(result.comapp[i][1]);
							}
						}
					}
					geraToPosting(res, idpost, listControl, userid, nameuser);
				});
			}else geraToPosting(res, idpost, listControl, userid, nameuser);
		}else geraToPosting(res, idpost, listControl, userid, nameuser);
	}
}

var geraToPosting = function(res, idPost, lista, userSrc, nameuser){
	console.verbose("", __module, __function, __line);
	console.debug("gerando to_posting para o post "+idPost, __module, __function, __line);
	var cont = 0;
	collection.findOne({"_id": new ObjectID(idPost)}, function(err, result) {
		if(result!==null && result!=undefined){
			for (var i = 0; i < lista.length; i++) {
				collectionToPosting.findOne({idpost: result._id.toHexString(), iduser:lista[i] }, function(err, resultPosting){
					var idSrc = ((userSrc===result.iduser_src)?undefined:userSrc);
					var nameSrc = ((userSrc===result.iduser_src)?undefined:nameuser);
					console.debug("id de origem do compartilhamento para to_posting: "+idSrc+", compartilhado por: "+userSrc+" postado por: "+result.iduser_src+" para o usuário "+lista[cont], __module, __function, __line);
					if(resultPosting!=undefined && resultPosting!= null && resultPosting._id != undefined){
						//colocado new Date() no created_time para fins de postagens ja criadas a algum tempo e compartilhadas posteriormente
						collectionToPosting.update({iduser:lista[cont], idpost:result._id.toHexString()},{$set:{iduser_src: idSrc, nameuser_src: nameSrc, created_time: new Date(), synchronized:0}}, {multi: true}, function(err, result2){});
					}else{
						collectionToPosting.insert({iduser:lista[cont], idpost:result._id.toHexString(), iduser_src: idSrc, nameuser_src: nameSrc, created_time: new Date(), idchannel:result.idchannel, synchronized:0}, function(err, result2){});
					}
					cont++;
				});
			}
			console.debug("gerou to_posting para "+lista.length+" usuários", __module, __function, __line);
			res.jsonp({'post' : {_id: idPost, status: lista.length} });
		}else{
			console.debug("post "+idPost+" não encontrado na base para gerar to_posting", __module, __function, __line);
			res.jsonp({'post' : {_id: idPost, status: "POST_NOT_EXIST"} });
		}
	});
}

/*
Não indicado mais de 100.000 na lista de usuários
*/
exports.toPostOneToMany = function(req, res){
	console.verbose("", __module, __function, __line);
	var body = '';
    req.on('data', function (data) {
        body += data;
    });

    req.on('end', function () {
    	// Pegando dados post
    	var userid = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'userid'))));
      	var serial = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'serialdevice'))));
    	var buildApp = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'build'))));
    	val.validatePost(userid, serial, buildApp, req, res,function(auth){
    		if(auth){
    			var lista = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'userlist')));
    			var idPost = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'idpost')));
		    	geraToPosting(res, idPost, lista);
		    }
		});
    });
}

exports.newComment = function(req, res){
	console.verbose("", __module, __function, __line);
	var body = '';
    req.on('data', function (data) {
        body += data;
    });

    req.on('end', function () {
    	// Pegando dados post
    	var userid = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'userid'))));
      	var serial = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'serialdevice'))));
    	var buildApp = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'build'))));
    	val.validatePost(userid, serial, buildApp, req, res,function(auth){
    		if(auth){
		    	var info_idpost = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'idpost')));
		    	var info_mensagem = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'message')));
		    	var info_nameuser = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'nameuser')));

		    	info_mensagem=val.validaInputUser(info_mensagem);

				collection.findOne({ _id: new ObjectID(info_idpost) }, function(err, result){
					if(!err && result!=undefined && result!= null){
						console.debug("post "+info_idpost+" recebeu novo comentario do usuario: "+info_nameuser, __module, __function, __line);
						var newC = {id: new ObjectID(), message: info_mensagem, iduser: userid, nameuser: info_nameuser, created_time: new Date() };
						if(result.comments==undefined){
							result.comments=[];
						}
						result.comments.push(newC);
						collection.update({ _id: new ObjectID(info_idpost) }, {$set:{comments: result.comments}},
				    		function(err, result2){
				    			collectionToPosting.update({idpost: info_idpost},{$set:{created_time: new Date(), synchronized:0}}, {multi: true}, function(err, result2){});
								res.jsonp({'post' : { _id: info_idpost, status: "OK" } });
							}
						);	
					}else{
						console.debug("post "+info_idpost+" não encontrado na base para gerar comentario do usuario: "+info_nameuser, __module, __function, __line);
						res.jsonp({'post' : {_id: info_idpost, status: "POST_NOT_EXIST"} });
					}
				});
			}
		});
    });
}

exports.removeComment = function(req, res){
	console.verbose("", __module, __function, __line);
	val.validateGet(req, res, function(req, res){
		var idPost = req.query.info_idpost;
		var idComment = req.query.info_idcomment;
		var params = {_id: new ObjectID(idPost)};
		collection.update({_id : new ObjectID(idPost)}, {$pull: {"comments" : { "id" : new ObjectID(idComment), "iduser": req.query.userid }}},
			function(err, result){
				collectionToPosting.update({idpost: idPost},{$set:{created_time: new Date(), synchronized:0}}, {multi: true}, function(err, result2){});
				res.jsonp({'post' : {_id: idPost, status: "OK"} });
			}
		);
	});
}