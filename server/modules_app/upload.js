/**
  Modulo de controle de uploads
*/
console.info("--> Carregou módulo upload");

exports.removeResourcePrePost = function(req, res){
  console.verbose("", __module, __function, __line);
  val.validateGet(req, res, function(req, res){
    var hash = req.query.hash;
    var userid = req.query.userid;
    if(hash){
      console.warn("removendo recurso alocado no servidor, hash: "+hash+", userid: "+userid, __module, __function, __line);
      hash = 'uploadpost/'+val.convertBase64ToString(hash);
      console.warn("nome do recurso a ser removido: "+hash, __module, __function, __line);
      if (fs.existsSync(hash)&&(!fs.lstatSync(hash).isDirectory())) {
          fs.unlink(hash, function (err) {
            if (err) res.jsonp({'response' : {file: hash, status: 'FILE_NOT_EXIST'}});
            console.debug("sucesso ao remover recurso :"+hash, __module, __function, __line);
            res.jsonp({'response' : {file: hash, status: 'OK'}});
            if (fs.existsSync(hash+'-small')&&(!fs.lstatSync(hash+'-small').isDirectory())) {
              fs.unlink(hash+'-small', function (err) {
                if (err) console.warn("falha ao remover small do recurso ["+hash+"-small]", __module, __function, __line);
                console.debug("sucesso ao remover small do recurso ["+hash+"-small]", __module, __function, __line);
              });
            }
            if (fs.existsSync(hash+'-thumb.png')&&(!fs.lstatSync(hash+'-thumb.png').isDirectory())) {
              fs.unlink(hash+'-thumb.png', function (err) {
                if (err) console.warn("falha ao remover thumb do recurso ["+hash+"-thumb.png]", __module, __function, __line);
                console.debug("sucesso ao remover thumb do recurso ["+hash+"-thumb.png]", __module, __function, __line);
              });
            }
          });
      } else { 
        console.error("falha ao remover recurso :"+hash+" o mesmo ja pode ter sido removido anteriormente", __module, __function, __line);
        res.jsonp({'response' : {file: hash, status: 'FILE_NOT_EXIST'}});
      }
    }else{
      res.jsonp({'response' : {file: hash, status: 'FILE_NOT_EXIST'}});
    }
  });
}

exports.uploadFile = function (req, res){
  console.verbose("", __module, __function, __line);
  var form = new formidable.IncomingForm();
  var file_name = '';
  var userid = '';
  var serialdevice = '';
  var typemedia = 'image/jpeg';
  var buildApp = '';
  form.parse(req);
  form.on('field', function(name, value) {
    if(name==='userid'){
      /* The file name of the uploaded file */
      userid=val.convertBase64ToString(val.decode(value));
      file_name=md5(userid);
    }else if(name==='serialdevice'){
      serialdevice=val.convertBase64ToString(val.decode(value));
    }else if(name==='typemedia'){
      typemedia=val.convertBase64ToString(val.decode(value));
    }else if(name==='build'){
      buildApp=val.convertBase64ToString(val.decode(value));
    }
  });
  form.on('end', function(fields, files) {
      /* Temporary location of our uploaded file */
      if(file_name===''){
        //file_name = this.openedFiles[0].name;
        res.jsonp({'upload' : 'ERROR'});
      }else{
        val.validatePost(userid, serialdevice, buildApp, req, res,function(auth){
          if(auth){
            /* Location where we want to copy the uploaded file */
            var temp_path = form.openedFiles[0].path;
            var new_location = req.route.path.substring(1)+'/';
            var isPost = (new_location==="uploadpost/");
            var preName = new Date().getTime();
            var ranToCheck = '';
            if(isPost){
              file_name=preName+file_name;
              ranToCheck=(new Buffer(file_name).toString('base64')).replace('=','');
            }
            console.debug("new location = "+new_location, __module, __function, __line);
            fse.copy(temp_path, new_location + file_name, function(err) {
              if (err) {
                console.error(err);
                res.jsonp({'upload' : 'ERROR'});
              } else {
                if (fs.existsSync('.cache/crop/50x50/'+new_location+file_name)) {
                  fs.unlinkSync('.cache/crop/50x50/'+new_location+file_name);
                }
                if(isPost){
                  if(typemedia==='image/jpeg'){
                    im.resize({
                      srcPath: new_location + file_name,
                      dstPath: new_location + file_name+'-small',
                      width:   1080
                    }, function(err, stdout, stderr){
                      res.jsonp({'upload' : {file: new_location+file_name, type: typemedia, hash: ranToCheck} });
                      if (err) throw err;
                      console.debug('resized '+new_location + file_name+' to width 500px');
                    });
                  }else{
                    ffmpeg(new_location + file_name).screenshots({
                      timemarks: [ '5%' ],
                      folder: new_location,
                      filename: file_name+'-thumb',
                      size: '500x500'
                    }).on("end", function(err) {
                      console.debug('upload de video '+new_location + file_name);
                      console.debug("criado com sucesso thumbnail para o arquivo de video: "+file_name, __module, __function, __line);
                      res.jsonp({'upload' : {file: new_location+file_name, type: typemedia, hash: ranToCheck} });
                    }).on("error", function(err) {
                      console.debug('upload de video '+new_location + file_name);
                      console.debug("erro ao criar thumbnail para o arquivo de video: "+file_name, __module, __function, __line);
                      console.debug(err, __module, __function, __line);
                      res.jsonp({'upload' : {file: new_location+file_name, type: typemedia, hash: ranToCheck} });
                    });
                  }
                }else{
                  res.jsonp({'upload' : {file: new_location+file_name, type: typemedia} });
                }
              }
            });
          }
        });
      }
  });
}

exports.getFileUploaded = function(req, res) {
  console.verbose("", __module, __function, __line);
  var request = url.parse(req.url, true);
  var path_min = req.route.path.substring(1)+'/';
  var file_img = path_min+req.query.i;
  file_img = decodeURIComponent(file_img);
  if (fs.existsSync(file_img)&&(!fs.lstatSync(file_img).isDirectory())) {
    var img = fs.readFileSync(file_img);
    res.writeHead(200, {'Content-Type': 'image/gif' });
    res.end(img, 'binary');
  } else { 
    res.writeHead(200, {'Content-Type': 'text/plain' });
    res.end('404 image not found.');
  }
}