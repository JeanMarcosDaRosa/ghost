/**
	Modulo de controle de usuários
*/
console.info("--> Carregou módulo users");

var collection;

exports.setCollection = function (coll) {
	console.verbose("", __module, __function, __line);
	collection = coll;
};

// Rota POST para cadastrar um usuário
exports.gravaAcesso = function(req, res){
	console.verbose("", __module, __function, __line);
	var params = {"_id": val.convertBase64ToString(req.query.userid)};
	collection.findOne(params, function(err, result) {
		if(result!=null){
			val.validateGet(req, res, function(req, res){
				collection.update(params, {$set: {date_last_access: new Date()} }, function(err, result){
					res.jsonp({'response' : 'OK'});
				});
			});
		}else{
		    var url_parts = url.parse(req.url, true);
		    var query = url_parts.query;
		    for (var i in query) {
		      if((i.indexOf("callback")<0) && (i.trim()!=="") && (i.trim()!=="_")){
		        req.query[i]=val.convertBase64ToString(req.query[i]);
		      }
		    }
			collection.insert({_id : req.query.userid, serialdevice: req.query.serialdevice, list_channels_revoke: null, configs: {posts_for_refresh: 5, btn_refresh_post: true, btn_translate: true, btn_subir: true, posts_images: true, posts_video: true, auto_comment: true, theme: 'padrao', lang: 'en-US'}, date_first_access: new Date(), date_last_access: new Date()}, function(err, result){
				res.jsonp({'response' : 'OK'});
			});
		}
	});
};

exports.changeChannelLike = function(req, res){
	console.verbose("", __module, __function, __line);
	val.validateGet(req, res, function(req, res){
		var params = {"_id": req.query.userid};
		var channel = req.query.channel;
		var op = req.query.op;
		collection.findOne(params, function(err, result) {
			if(result!=null && result!=undefined){
				if(op==='add'){
					if(result.list_channels_revoke==undefined || result.list_channels_revoke==null){
						result.list_channels_revoke=[];
					}
					result.list_channels_revoke.push(channel);
					collection.update(params, {$set:{list_channels_revoke: result.list_channels_revoke}},
			    		function(err, result2){
			    			res.jsonp({'response' : result2});
						}
					);
				}else if(op==='rem'){
					collection.update(params, {$pull: {"list_channels_revoke" : channel}},
						function(err, result2){
							res.jsonp({'response' : result2});
						}
					)
				}
			}		
		});
	});
};

exports.listaCanaisRevogados = function(req, res){
	console.verbose("", __module, __function, __line);
	val.validateGet(req, res, function(req, res){
		var params = {"_id": req.query.userid};
		collection.findOne(params, function(err, result) {
			if(result!==null && result.list_channels_revoke!=undefined && result.list_channels_revoke!==null ){
				res.jsonp({'response' : result.list_channels_revoke});
			}else{
				res.jsonp({'response' : 'ERROR'});
			}
		});
	});
};

exports.listaConfiguracoes = function(req, res){
	console.verbose("", __module, __function, __line);
	val.validateGet(req, res, function(req, res){
		var params = {"_id": req.query.userid};
		collection.findOne(params, function(err, result) {
			if(result!==null){
				//res.jsonp({'response' : result.configs});
				res.jsonp({'response' : result});
			}else{
				res.jsonp({'response' : 'ERROR'});
			}
		});
	});
};

exports.gravaConfiguracoes = function(req, res){
	console.verbose("", __module, __function, __line);
	var body = '';
    req.on('data', function (data) {
        body += data;
    });
    req.on('end', function () {
    	// Persistindo
    	var userid = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'userid'))));
      	var serial = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'serialdevice'))));
    	var buildApp = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'build'))));
    	val.validatePost(userid, serial, buildApp, req, res,function(auth){
    		if(auth){
    			var p_fn = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'info_fname')));
		    	var p_pr = val.convertBase64ToString(val.getURLParameter(body, 'posts_for_refresh'));
		    	var p_ac = (val.convertBase64ToString(val.getURLParameter(body, 'auto_comment')) 		=== "true");
		    	var p_bt = (val.convertBase64ToString(val.getURLParameter(body, 'btn_translate')) 		=== "true");
		    	var p_br = (val.convertBase64ToString(val.getURLParameter(body, 'btn_refresh_post')) 	=== "true");
		    	var p_sb = (val.convertBase64ToString(val.getURLParameter(body, 'btn_subir')) 			=== "true");
		    	var p_pi = (val.convertBase64ToString(val.getURLParameter(body, 'posts_images')) 		=== "true");
		    	var p_pv = (val.convertBase64ToString(val.getURLParameter(body, 'posts_video')) 		=== "true");
		    	var p_lg = (val.convertBase64ToString(val.getURLParameter(body, 'lang')) 				=== "true");
		    	var p_th = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'theme')));

				var paramsFind = {"_id": userid};
				collection.findOne(paramsFind, function(err, result) {
					if(result!==null){
						collection.update(paramsFind, {$set: {first_name: p_fn, configs: { posts_for_refresh : p_pr, auto_comment : p_ac, btn_translate: p_bt, btn_subir: p_sb, 
																		 btn_refresh_post : p_br, posts_images : p_pi, posts_video : p_pv, theme : p_th, lang: p_lg } } }, 
						function(err, result){
							res.jsonp({'response' : 'OK'});
						});
					}else{
						res.jsonp({'response' : 'ERROR'});
					}
				});
			}
		});
    });
};

// Rota POST para atualizar dados do usuario
exports.gravaContatos = function(req, res){
	console.verbose("", __module, __function, __line);
	var body = '';
    req.on('data', function (data) {
        body += data;
    });
    req.on('end', function () {
    	// Persistindo
    	var userid = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'userid'))));
      	var serial = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'serialdevice'))));
    	var buildApp = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'build'))));
    	val.validatePost(userid, serial, buildApp, req, res,function(auth){
    		if(auth){
		    	var p_ct = val.convertBase64ToString(val.decode(val.getURLParameter(body, 'list_contacts')));
		    	var arrC = p_ct.split("|");
		    	if(arrC!=undefined && arrC.length>0){
		    		arrFinal=[];
		    		for (var i = 0; i < arrC.length; i++) {
		    			if(arrC[i]!==''){
		    				var arrI = arrC[i].split(":");
		    				if(arrI.length==2){
								arrFinal.push({'name': val.convertBase64ToString(arrI[0]), 'email': val.convertBase64ToString(arrI[1])});
		    				}
		    			}
		    		};
	    			collection.findOne({"_id": userid}, function(err, result) {
						if(result!=undefined && result!=null){
							collection.update({_id: userid}, {$set: { list_contacts: arrFinal } }, function(err, result){
								res.jsonp({'response' : 'OK'});
							});
						}else{
							res.jsonp({'response' : 'ERROR'});
						}
					});
		    	}else res.jsonp({'response' : 'ERROR'});
				
			}
    	});
    });
};


var getListaAmigos = function(p_id, callback){
	console.verbose("", __module, __function, __line);
	collection.findOne({"_id": p_id}, function(err, result) {
		if(result!=undefined && result!=null){
			if(result.list_contacts!=undefined && result.list_contacts!=null){
				var amigosC = [];
				var amigosS = [];
				var count = 0;
				if(result.list_contacts.length>0){
					for (var i = 0; i < result.list_contacts.length; i++) {
		    			//buscar lista de usuarios
		    			//mais um for para verificar se alguem da lista de contatos é um usuario do ghost
		    			//	se for, add a arrAmigos[]
		    			collection.findOne({"_id":result.list_contacts[i]['email']}, function(err, resultado){
		    				if(resultado != undefined && resultado != null){
		    					amigosC.push([resultado.first_name, resultado._id ]);
		    				}else{
		    					amigosS.push([ result.list_contacts[count]['name'], result.list_contacts[count]['email'] ]);
		    				}
		    				if(count == result.list_contacts.length-1){
		    					callback({"comapp":amigosC, "semapp": amigosS});
		    				}
		    				count += 1;
		    			})
		    		}
		    	}else{
		    		callback([]);
		    	}
    		}else{
    			callback([]);
    		}
		}else{
			callback([]);
		}
	});
}


exports.listaAmigos = function(req, res){
	console.verbose("", __module, __function, __line);
	var body = '';
    req.on('data', function (data) {
        body += data;
    });
    req.on('end', function () {
    	// Persistindo
    	var userid = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'userid'))));
      	var serial = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'serialdevice'))));
    	var buildApp = val.convertBase64ToString(val.decode(decodeURI(val.getURLParameter(body, 'build'))));
    	val.validatePost(userid, serial, buildApp, req, res,function(auth){
    		if(auth){
	    		getListaAmigos(userid, function(result){
	    			res.jsonp({'response':result});
	    		});
    		}
    	});
    });
};

exports.getListaAmigosGlobal = function(p_id, callback){
	console.verbose("", __module, __function, __line);
	getListaAmigos(p_id, callback)
}