/**
	Modulo de controle de channels
*/
console.info("--> Carregou módulo channels");

var collection;

exports.setCollection = function (coll) {
	console.verbose("", __module, __function, __line);
	collection = coll;
	checkFriendsChannel();
	checkGhostChannel();
}

// Rota GET para listar os canais
exports.list = function(req, res){
	console.verbose("", __module, __function, __line);
	val.validateGet(req, res, function(req, res){
		// Fazendo uma consulta no banco de dados
		collection.find().sort({name: 1}).toArray(function(err, result){
			if(result!=undefined && result!=null){
				var amigos = [];
				for (var i = 0; i < result.length; i++) {
					if(result[i].username==="friends"){
						amigos = result[i];
						result.splice(i, 1);
						break;
					}
				}
				result.unshift(amigos);
			}
			res.jsonp({'channels' : result});
		});
	});
}

exports.getChannel = function(callback, idchannel){
	console.verbose("", __module, __function, __line);
	// Fazendo uma consulta no banco de dados
	var params = {_id: new ObjectID(idchannel)};
	collection.findOne(params, function(err, result){
		callback(result);
	});
}

exports.getChannelByName = function(callback, usernameChannel){
	console.verbose("", __module, __function, __line);
	// Fazendo uma consulta no banco de dados
	var params = {username: usernameChannel};
	collection.findOne(params, function(err, result){
		callback(result);
	});
}

var checkFriendsChannel = function(){
	console.verbose("", __module, __function, __line);
	//Testando se existe o canal friends na lista
	var params = {username: 'friends'};
	collection.findOne(params, function(err, result){
		if(result!=null && result!=undefined && result.username!=null && result.username!=undefined && result.username==='friends'){
			console.debug("Canal friends encontrado na collection channels", __module, __function, __line);
		}else{
			console.warn("Canal friends não encontrado na collection channels", __module, __function, __line);
			collection.insert({description : "Postagem de usuario", name : "Postagem de Contatos", username : "friends"}, function(err, result){
				console.debug("Canal friends foi inserido na collection channels com sucesso", __module, __function, __line);
			});
		}
	});	
}

var checkGhostChannel = function(){
	console.verbose("", __module, __function, __line);
	//Testando se existe o canal ghost na lista
	var params = {username: 'ghost'};
	collection.findOne(params, function(err, result){
		if(result!=null && result!=undefined && result.username!=null && result.username!=undefined && result.username==='ghost'){
			console.debug("Canal ghost encontrado na collection channels", __module, __function, __line);
		}else{
			console.warn("Canal ghost não encontrado na collection channels", __module, __function, __line);
			collection.insert({description : "Marketing", name : "Ghost App", username : "ghost"}, function(err, result){
				console.debug("Canal ghost foi inserido na collection channels com sucesso", __module, __function, __line);
			});
		}
	});	
}