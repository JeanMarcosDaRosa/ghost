import goslate
import sys
import os
import base64

def tradutor(text, lang):
 try:
  gs = goslate.Goslate()
  return gs.translate(text, lang)
 except Exception, e:
  return 'SMARTFEEDTRANSLATEFAIL1'


if len(sys.argv) >= 3:
 lang = sys.argv[1]
 cnt=0
 text=[]
 for arg in sys.argv:
  cnt=cnt+1
  if cnt>2 :
   text.append(arg)
 try:
  if len(text)>0 and lang != '':
   for t in text:
    print base64.b64encode(tradutor(base64.b64decode(t), base64.b64decode(lang)).encode('utf-8'))
  else:
   print 'SMARTFEEDTRANSLATEFAIL2'
 except Exception, e:
  print 'SMARTFEEDTRANSLATEFAIL3'
else:
 print 'SMARTFEEDTRANSLATEFAIL4'
