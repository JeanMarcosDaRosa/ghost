import time
import sys
import os
import httplib
import urllib2
import json
#Email
import smtplib
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart

def serverNode():
	global host, port, enviaEmailDB
	try:
		link = httplib.HTTPConnection(host, int(port))
		link.connect()
		link.request('GET', '/status')
		response = link.getresponse()
		if response.status == httplib.OK:
		    result = json.loads(response.read())
		    if result["status"]["DB"]=="OK":
		    	enviaEmailDB=True
		    else:
		    	print "Erro no banco de dados"
		    	if enviaEmailDB :
		    		enviarEmail('Falha no banco de dados', '', 0)
		    	enviaEmailDB=False
		    return True
		else:
			return None
	except Exception, e:
		print 'Fail to connect in server ', link, ' [', e, '] '
		
	return None

# Faz um teste de 3 em 3 segundo durante 3 vezes, caso nao retornar true vai 
# para funcao shell
def funcaoAnteEmail():
	for i in range (0,3):
		if serverNode() != None:
			return True
		time.sleep(3)

	shell()
	
def obtemArquivoLog():
	#carregando arquivo de log
	try:
		arq = open('server.log', 'r')
		log = arq.read()
		return str(log)
	except Exception, e:
		print 'Erro ao carregar arquivo de log [', e, ']'


def modificacaoNaoPlanejada():
	# verifica se o processo esta ativo, caso esteja, nesse caso
	# ele nao esta respondendo, entao na linha 62 ele mata o
	# processo
	p = os.popen('ps aux | grep -i ghostserver.js',"r")
	line = []
	while 1:
		temp = p.readline()
		line.append(temp)
		if not temp:
			break

	
	# Mato o processo do nodejs caso estja rodando mas nao respondendo
	if ( line[0].split(' ')[23] == 'nodejs' or line[0].split(' ')[23] == 'node' ) and line[0].split(' ')[24].rstrip() == 'ghostserver.js' :
		print '\n\n\t\t*** Servico encontrado ***\n'
		print '{ ', line[0].rstrip(), ' }'
		print '\t<<< Matando o servico do NodeJS >>>\n'
		print ' > Processo [', line[0].split(' ')[23], ']'
		print ' > Nome d0 Processo [', line[0].split(' ')[24].rstrip(), ']'
		print ' > ID [', line[0].split(' ')[6], ']'

		mata = os.popen('kill '+line[0].split(' ')[6])


	# Inicia o servico do nodejs
	print '\t** Iniciando os servico **'
	node = os.popen('nodemon ghostserver.js '+( '--debug' if onDebug else '')+' > server.log &')
	

# Funcao responsavel por enviar chamar a funcao de enviar email, 
# matar o processo nodejs caso ele nao esteja respondendo e levantar o 
# servico do node
def shell():
	global enviaEmail

	if enviaEmail == True:
		enviarEmail('\n\n\t*** Servidor Ghost caiu, processo guardiao ira levantar o processo! ***', obtemArquivoLog(), 0)

	modificacaoNaoPlanejada()
			
	# espera dez segundo para verificar se o servico esta rodando
	time.sleep(10)
	if serverNode() == None:
		print 'servico ainda nao respondendo'
		if enviaEmail == True:
			if enviaEmail == True:
				enviarEmail('\n\n\t*** Servidor Ghost caiu, processo guardiao nao conseguiu levantar o servico nodejs ghostserver.js. ***\n\t\t <<< Por favor, verifique o servidor!! >>>', obtemArquivoLog() ,1)	
	else:
		enviarEmail('\n\n\t*** Processo guardiao conseguiu levantar o servico nodejs ghostserver.js. ***\n\t\t <<< Por favor, verifique o servidor!! >>>', '', 1)	
	return True


def enviarEmail(obs, log, v):

	print 'Enviando email'
	global enviaEmail
	global emails
	global noMail

	if noMail == False:
		COMMASPACE = ', '

		fromaddr = 'contato@jeanmarcos.com.br'
		#Email Principal
		toaddr = emails[0]

		#Criando lista de email de quem recebera as copias
		listCopyEmails = []
		cont = 0
		for e in emails:
			if cont > 0:
				listCopyEmails.append(e)
			cont += 1
		
		msg = MIMEMultipart()
		msg ['Subject'] = 'Erro no servidor NodeJS'
		#msg ['From'] = 'Ghost'
		msg ['To'] = toaddr
		msg ['CC'] = COMMASPACE.join(listCopyEmails)
		corpoEmail = 'Servidor node nao esta respondendo\n\tPor favor verifique o servidor!\n\nNao responda, este eh apenas um email de alerta!'
		corpoEmail += obs
		corpoEmail += '\n\n-------------- *** LOGS *** -------------\n' 
		corpoEmail += log
		msg.attach(MIMEText(corpoEmail, 'plain'))
		toaddrs = [toaddr]+listCopyEmails # criando uma lista de envio

		# Credenciais necessarias
		username = 'contato@jeanmarcos.com.br'
		password = 'nobug95cross95'

		# Enviando email
		try:
			server = smtplib.SMTP('jeanmarcos.com.br:587')
			# server.starttls() # usado em contas do google (Tunneled Transport Layer Security), ou seja, com uso do ttls
			# os dados como usuario e senha sao transportados de forma criptografada ao servidor, o que nao acontece nos
			# servidores de email
			server.login(username,password)
			server.sendmail(fromaddr, toaddrs, msg.as_string())
			server.quit()
		except Exception, e:
			print 'Erro ao enviar email [', e,']'

		# Apos enviar o primeiro email, nao enviara mais nenhum, a nae ser que o servidor
		# volte a rodar e depois falhe novamente
		if v == 1:
			enviaEmail = False	
	else:
		print '\n\tConfigurado para nao enviar email!\n'

def verificaComandos(comand):
	comandos = ['-t', '-h', '-host', '-port', '--no-mail', '--debug']
	for c in comandos:
		if c == comand:
			return True

	return False

def ajuda():
	print '\n\t__---**** Exibindo menu de ajuda ****---__\n'
	print ' * -t {Informar o tempo de verificao entre uma verifica e outra do guardiao}'
	print '\t ex: python guardiao.py -t 10\n'
	print ' * -host {Informar o host que sera monitorado}'
	print '\t ex: python guardiao.py -host 127.0.0.1\n'
	print ' * -port {Informar a prta que sera monitorada}'
	print '\t ex: python guardiao.py -port 8081\n'
	print ' * --no-mail {nao enviar email - util em caso de teste no servidor}'
	print '\t ex: python guardiao.py --no-mail\n'

os.system('clear')
os.system('figlet Gardiao esta sendo executado')

# variaveis globais
enviaEmail = True
enviaEmailDB = True
emails = []
noMail = False
onDebug = False

# carregando lista de email do arquivo
arq = file('emails.txt', 'r')
while True:
	linha = arq.readline()
	if len(linha) == 0:
		break #Fim do arquivo

	emails.append(linha.rstrip()) # rstrip() -> limpa \n e \r

# verifica se ha emails, caso nat tenha nenhum email entao o resto do programa nao executa
if len(emails) == 0:
	print 'Informe pelo menos 1 email no arquivo \'emails.txt\''
	exit(1)

# Exibe a lista de emails que foi lida do arquivo
print '\n\t__--*** Lista de email carregada ***--__'
for e in emails:
	print ' * ', e
print '\t <<< Total de emails: [', len(emails), '] >>>\n'

tempo = 5 # caso nao passdo, tempo padrao eh de cinco em cinco segundos verificar se o servidor esta funcionando
host = '127.0.0.1' # host padrao caso nao seja passado por parametro
port = 8080 # porta padrao caso nao seja informado por parametro

# Recebendo argumentos do terminal
cont = 1
if len(sys.argv) > 1:
	for arg in sys.argv:
		if arg != sys.argv[0]:
			if verificaComandos(arg) == True:
				if arg == '-t':
					if len(sys.argv) <= cont:
						print 'Falta o valor inteiro do tempo!'
						exit(1)
					tempo = int(sys.argv[cont])
				elif arg == '-h':
					ajuda()
					exit(1)
				elif arg == '-host':
					if len(sys.argv) <= cont:
						print 'Falta o endereco do host!'
						exit(1)
					host = sys.argv[cont]
				elif arg == '-port':
					if len(sys.argv) <= cont:
						print 'Falta a porta do servidor!'
						exit(1)
					port = sys.argv[cont]
				elif arg == '--no-mail':
					noMail = True
				elif arg == '--debug':
					onDebug = True
		cont += 1
else:
	print '-h para ajuda!'
	exit(1)
print '\n\t <<< Parametros informados >>>'
print ' * Tempo de verificacao: [', tempo, 'seg]'
print ' * Host [',host,']'
print ' * Porta [', port,']'
print ' * Nao Enviar Email? [', str(noMail),']'
print '----------\n'

# Iniciando o servico
modificacaoNaoPlanejada()

while True:
	if serverNode() != None:
		print '\r Server OK [',time.ctime(),']',
		sys.stdout.flush()
		enviaEmail = True
		time.sleep(tempo)

	else:
		print '\n---------\n'
		funcaoAnteEmail()