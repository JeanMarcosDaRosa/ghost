/**
  Modulo principal de controle de fluxo
*/

//bloco de requires
os          = require('os');
colors      = require('colors');
express     = require('express');
app         = express();
MongoClient = require('mongodb').MongoClient;
ObjectID    = require('mongodb').ObjectID;
BSON        = require('mongodb').BSONPure;
format      = require('util').format;
qt          = require('quickthumb');
fs          = require("fs");
url         = require("url");
domain      = require('domain').create();
nodemailer  = require('nodemailer');
iconv       = require('iconv');
http        = require("http");
path        = require("path");
exec        = require('child_process').exec;
formidable  = require('formidable');
util        = require('util');
fse         = require('fs-extra');
md5         = require('MD5');
im          = require('imagemagick');
ic          = new iconv.Iconv('iso-8859-1', 'utf-8');
ffmpeg      = require('fluent-ffmpeg');
jsdom       = require('jsdom');

//variaveis de uso geral
sistema     = os.type().toLowerCase();
portRest    = 8080; //sem o prefixo 'var', a variavel torna-se global entre os modulos
portHttp    = 8081;
qtdLogs     = 4096;
inDEBUG     = false;
logsSystem  = [];
verbose     = true;
debug       = true;
buildMin    = false;
enableStatus= false;
enableUpdate= false;
startTime   = new Date();
checkMD5    = '03024031872c230d60f5557f4306e7a5'; //tHe_SoCiALgHoSt 

// set theme
colors.setTheme({
  verbose: 'cyan',
  info: 'green',
  warn: 'yellow',
  debug: 'blue',
  help: 'blue',
  error: 'red'
});

//Definição dos consoles e cores
console.info    = function(message, module, functionBy, line){message=message+(module?"["+module+"] ":"")+(functionBy?"["+functionBy+"] ":"")+(line?"line ["+line+"] ":"");                              addOnLogSystem(message); console.log(colors.info(message));     }
console.error   = function(message, module, functionBy, line){message=(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''))+" [error]: "+(module?"["+module+"] ":"")+(functionBy?"["+functionBy+"] ":"")+(line?"line ["+line+"] ":"")+message;            addOnLogSystem(message); console.log(colors.error(message));    }
console.verbose = function(message, module, functionBy, line){message=(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''))+" [verbose]: "+(module?"["+module+"] ":"")+(functionBy?"["+functionBy+"] ":"")+(line?"line ["+line+"] ":"")+message; if(inDEBUG && verbose){ addOnLogSystem(message); console.log(colors.verbose(message)); } }
console.warn    = function(message, module, functionBy, line){message=(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''))+" [warn]: "+(module?"["+module+"] ":"")+(functionBy?"["+functionBy+"] ":"")+(line?"line ["+line+"] ":"")+message; if(inDEBUG){ addOnLogSystem(message); console.log(colors.warn(message));    } }
console.debug   = function(message, module, functionBy, line){message=(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''))+" [debug]: "+(module?"["+module+"] ":"")+(functionBy?"["+functionBy+"] ":"")+(line?"line ["+line+"] ":"")+message; if(inDEBUG && debug){ addOnLogSystem(message); console.log(colors.debug(message));   } }
console.help    = function(message){ console.log(colors.debug(message)); }

var addOnLogSystem = function(message){
    if(logsSystem.length>=qtdLogs){
        logsSystem.splice(0, 1);
    }
    logsSystem.push(message);
}

//Acessando o V8 para melhor debug
Object.defineProperty(global, '__stack', {
get: function() {
        var orig = Error.prepareStackTrace;
        Error.prepareStackTrace = function(_, stack) {
            return stack;
        };
        var err = new Error;
        Error.captureStackTrace(err, arguments.callee);
        var stack = err.stack;
        Error.prepareStackTrace = orig;
        return stack;
    }
});

Object.defineProperty(global, '__line', {
get: function() {
        return __stack[1].getLineNumber();
    }
});

Object.defineProperty(global, '__function', {
get: function() {
        return __stack[1].getFunctionName();
    }
});

Object.defineProperty(global, '__module', {
get: function() {
        if (__stack[1].getFileName()){
            var nome = __stack[1].getFileName().split("/");
            return nome[nome.length-1];
        }else return "";
    }
});

//Rota de erro
domain.on('error', function(err){
    console.verbose("Function on error domain");
    console.error(err.stack);
});

var showHelp = function(){
	console.help('Usage:');
	console.help('\t--debug | -d to enable debug mode on the server, displays all messages generated');
	console.help('\t--qtdlogs | -qL [value] to set the size of the list of messages that must be stored to requests for route /status');
	console.help('\t--no-verbose-log | -nVL to not display the verbose type logs, which are mostly incoming messages in functions');
	console.help('\t--no-debug-log | -nDL to not display the type debug logs that are more detailed');
    console.help('\t--build-min | -b defines the required minimum version of the application');
    console.help('\t--enable-status | -s enable the route /status, which allows monitoring by the browser');
    console.help('\t--enable-update | -u enable the route /update, which allows the server update by browser');
    console.help('\t--hash | -c hash for authentication of update and status routes, if not informed, makes use of a standard hash)');
	console.help('\t--help | -h show this menu');
}

// Quick and dirty 

function extractOpenGraph(url, fn) {
    var og = [];
    jsdom.env(
        url,
        ["http://code.jquery.com/jquery.js"],
        function (errors, window) {
            window.$('meta[property^=og]').each(function(i, tem) {
                og.push([ tem.getAttribute('property'), tem.getAttribute('content')]);
            });
            fn(og);
        }
    );
}


domain.run(function() {
    console.info('\n    """""""--....... Server Ghost .......--"""""""');
    console.info('/-----------------------------------------------------\\');
    console.info('Begin data time : \t'+new Date());
    console.info('Operation System: \t'+sistema);
    var isHelp = false, nextIsLog = false, nextIsBuild = false, nextIsMd5 = false, changeMD5 = false, params = false;
    process.argv.forEach(function (val, index, array) {
      if(index>=2){
        params = true;
        console.info('Parameter found: \t' + val);
        if(nextIsLog){
            qtdLogs = parseInt(val,10);
            nextIsLog=false;
        }else if(nextIsBuild){
            buildMin = parseInt(val,10);
            nextIsBuild=false;
        }else if(nextIsMd5){
            checkMD5 = md5(val);
            nextIsMd5=false;
            changeMD5=true;
        }else{
            if(val.toLowerCase()==="--debug"            || val.toLowerCase()==="-d")    inDEBUG      = true;
            if(val.toLowerCase()==="--qtdlogs"          || val.toLowerCase()==="-qL")   nextIsLog    = true;
            if(val.toLowerCase()==="--no-verbose-log"   || val.toLowerCase()==="-nVL")  verbose      = false;
            if(val.toLowerCase()==="--no-debug-log"     || val.toLowerCase()==="-nDL")  debug        = false;
            if(val.toLowerCase()==="--build-min"        || val.toLowerCase()==="-b")    nextIsBuild  = true;
            if(val.toLowerCase()==="--enable-status"    || val.toLowerCase()==="-s")    enableStatus = true;
            if(val.toLowerCase()==="--enable-update"    || val.toLowerCase()==="-u")    enableUpdate = true;
            if(val.toLowerCase()==="--hash"             || val.toLowerCase()==="-c")    nextIsMd5    = true;
            if(val.toLowerCase()==="--help"             || val.toLowerCase()==="-h"){
            	console.info('\\-----------------------------------------------------/');
            	showHelp();
            	isHelp = true;
            }
        }
      }
    });
    
    if(isHelp) process.exit(code=0);
    
    console.info('Hash for authentication: '+checkMD5);

    if(!params) showHelp();

    //parametros do servidor
    app.set("jsonp callback", true);
    app.enable("jsonp callback");

    app.use(function (req, res, next) {
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');
        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);
        // Pass to next layer of middleware
        next();
    });

    //Importando modulos
    console.info("Modules------------------------------------------------");
    val = require('./modules_app/validations');
    users = require('./modules_app/users');
    channels = require('./modules_app/channels');
    posts = require('./modules_app/to_posting');
    upload = require('./modules_app/upload');
    
    //conexao com o banco de dados
    MongoClient.connect('mongodb://127.0.0.1:27017/ghost', function(err, db) {
    	if(err) throw err;
        val.setCollectionUser(db.collection('users'));
    	users.setCollection(db.collection('users'));
    	channels.setCollection(db.collection('channels'));
    	posts.setCollection(db.collection('posts'));
        posts.setCollectionToPosting(db.collection('to_posting'));
        posts.setCollectionUser(db.collection('users'));
    });

    app.use('/public',                  qt.static(__dirname + '/'));
    app.get('/status',                  val.getStatusServer);
    app.get('/update',                  val.execUpdate);
    app.get('/timerdb',                 val.getTimer);
    app.get('/confirmtoken',            val.authNewToken);
    app.get('/image',                   upload.getFileUploaded);
    app.get('/accessRegister',          users.gravaAcesso);
    app.get('/changechannel',           users.changeChannelLike);
    app.get('/getRevokedChannels',      users.listaCanaisRevogados);
    app.get('/listconfigs',             users.listaConfiguracoes);
    app.post('/saveconfigs',            users.gravaConfiguracoes);
    app.post('/savecontacts',           users.gravaContatos);
    app.post('/listfriends',            users.listaAmigos);
    app.post('/upload',                 upload.uploadFile);
    app.post('/uploadpost',             upload.uploadFile);
    app.get('/removeprepost',           upload.removeResourcePrePost);
    app.get('/channels',                channels.list);
    app.get('/lastposts',               posts.listLast);
    app.get('/newposts',                posts.listNews);
    app.get('/postsConfirm',            posts.postsConfirm);
    app.get('/getpost',                 posts.getPost);
    app.get('/removecomment',           posts.removeComment);
    app.post('/createpost',             posts.newPost);
    app.post('/newcomment',             posts.newComment);
    app.post('/topostonetomany',        posts.toPostOneToMany);
    app.post('/compartilharpost',       posts.sharePost);
    app.post('/removepost',             posts.removePost);
    app.get('/search',                  posts.search);

    app.get('/og', function(req, res){
        if (req.query.url) {
            var url = req.query.url;
            extractOpenGraph(url, function(data) {
                res.jsonp({url: url, data: data});
            });
        }
    });

    //coloca o servidor REST para rodar na porta pre-definida
    app.listen(portRest);

    //mostra o status do servidor nodejs
    console.info("Services-----------------------------------------------");
    console.info('HTTP port: \t'+portHttp);
    console.info('REST port: \t'+portRest);
    console.info('\\-----------------------------------------------------/');
});