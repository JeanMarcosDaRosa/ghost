#!/usr/bin/python
import os
import shutil
import getpass

def readFile(filename):
	f = open(filename, 'r')
	r=f.read()
	f.close()
	return r

def writeFile(filename, str):
	f = open(filename, 'w')
	f.write(str)
	f.close()

def changebuild(file_build):
	buildnumber=readFile(file_build)
	newnumber=buildnumber[buildnumber.index('=')+1:-1]
	if newnumber[-1]==';': newnumber=newnumber[:-1]
	newnumber=(int(newnumber)+1)
	var='BUILD_NUMBER='+`newnumber`+';'
	writeFile(file_build, var)
	return newnumber

def printText(txt):
	lines = txt.split('\n')
	for line in lines:
		print line.strip()
		
sdk_android = "/home/"+getpass.getuser()+"/eclipse-luna/android-sdk-linux/tools"
os.environ["PATH"] = os.environ["PATH"] + ":" + sdk_android
path = os.getcwd()+'/'
file_build=path+'app/ghost/platforms/android/assets/www/js/buildnumber.js'
path_debug=path+'app/ghost/platforms/android/assets/www/js/debug.js'
buildversion=readFile(file_build)
teste = raw_input('Gerar APK?:[S,n] ')
if teste == 'S':
	print "Para o correto funcionamento, certifique-se de que o sdk android encontra-se em:",sdk_android
	debug = readFile(path_debug)
	writeFile(path_debug, "DEBUG=false;")
	print "Gerando o APK Android do ghost... [build --release]"
	cmd=path+'app/ghost/platforms/android/cordova/build --release'
	os.system(cmd)
	print "Copiando apk para app/ghost.apk"
	shutil.copyfile(path+'app/ghost/platforms/android/ant-build/Ghost-release-unsigned.apk', path+'app/ghost.apk')
	print "assinando o novo apk [jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ghost-release-key.keystore app/ghost.apk ghost]"
	os.system('jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore '+path+'res/keystore/ghost-release-key.keystore -storepass SocialGhostApp -keypass SocialGhostApp '+path+'app/ghost.apk ghost')
	print "apk assinado e gerado em [app/ghost/platforms/android/bin/Ghost.apk]"
	writeFile(path_debug, debug)
	print "modificando versao de build de:", buildversion
	buildversion=changebuild(file_build)
	print "para:", buildversion
	print "Geracao finalizada!"
print "Adicionando todos os arquivos que estao fora do repositorio"
os.system('git add --all :/')
MSG = raw_input('Digite suas alteracoes: ')
print "Commitando alteracoes no repositorio"
os.system('git commit -am "build: '+str(buildversion)+' -> '+MSG+'"')
teste = raw_input('Enviar para o repositorio?:[S,n] ')
if teste == 'S':
	os.environ["GIT_ASKPASS"] = '/usr/bin/ksshaskpass'
	os.system('git config --global core.askpass /usr/bin/ksshaskpass')
	print 'O proximo passo ira enviar os arquivos para o repositorio, tenha o ksshaskpass instalado [apt-get install ksshaskpass]'
	print "Enviando para o repositorio remoto: "
	os.system('git push origin master')
	os.environ["GIT_ASKPASS"] = ''
	teste = raw_input('Requisitar update no servidor remoto da aplicacao?:[S,n] ')
	if teste == 'S':
		import httplib
		import urllib2
		httpServ = httplib.HTTPConnection("162.243.69.60", 8080)
		httpServ.connect()
		hash_str="03024031872c230d60f5557f4306e7a5"
		teste = raw_input('Usar hash padrao?:[S,n] ')
		if teste != 'S':	
			import hashlib
			m = hashlib.md5()
			hash_str = raw_input('Digite a palavra que forma a hash: ')
			m.update(hash_str)
			hash_str = m.hexdigest()
			print "Hash: ", hash_str
		httpServ.request('GET', '/update?hash={0}&copyapp={1}'.format(hash_str, 'sim'), '')
		response = httpServ.getresponse()
		if response.status == httplib.OK:
			print "Servidor respondeu: "
			printText (response.read())
		else:
			print "Falha ao aguardar resposta do servidor, ele pode estar demorando para atualizar, ou a hash de autenticacao esta incorreta, verifique."
print "Pronto."